# Evolutionary Neural network

## Description
What it says on the tin! This is a demonstration of training a neural network via a evolutionary algorithm with mutation, selection, and sexual reproduction.

## Interface
This is the code to create a neural network:

    int id = 0;
    Neural_Lattice brain;

    // add input cluster
    brain.cluster.emplace_back();

    // the id should be unique
    brain.cluster.back().set_id(id++);

    // add neurons to the cluster
    brain.cluster.back().grow_neuron(n_neurons);

    // grow axons however you like - in this case, all neurons are wired to
    // all other neurons
    for (auto & neuron : brain.cluster[0].neurons)
        for (auto & n_target : brain.cluster[0].neurons)
            neuron->grow_axon(n_target.get());

Push values into input neurons:

    brain.cluster[0].neurons[i]->push_input(value);

Trigger the neural network, making each neuron in it fire (or not) depending on their configurations:

    brain.pulse(n);

The variable *n* simply causes the neural network to pulse *n* times.

After the neural network has spent *n* cycles thinking, you can read the output:

    for (auto & neuron : brain.cluster[0].neurons) {
        auto value = neuron->get_value();
        // do something with the value
    }

A child neural network (recombined from the parents & mutated) can be produced:

    auto brain_child = Neural_Network(brain_male, brain_female);

## Examples
Probably the neatest example:

![Avoid the Walls](examples/dots-short.gif)

The environment the neural networks have evolved to in that example is this:

* The dots (organisms) live on a field with four walls
* Touching the walls yields instant death, *i.e.* the dot can no longer gather food and earn points
* The dots have a maximum lifespan
* Food will appear at a random point on the field
* Eat food by bumping into it
* Food respawns after some time at a random point on the field
* Points for movement
* Lots of points for eating (bumping into) food

Obviously, the best strategy in this scenario is to head straight for the closest bit of food. It's kind of neat to watch them learn this.

One interesting effect I've observed is that the dots will often be very indecisive, early in their evolution, when they find themselves in the middle of two bits of food (differently colored). They have separate input neurons dedicated to seeing each color (blue and red), and, in an amusing bit of "survival of the goodest-enough", what generally happens is that the dots will evolve to only eat either red or blue food. This breaks the indecisiveness, which is apparently a big enough strategic advantage to make ignoring the other color of food worthwhile.

While that's a suboptimal solution that evolves quickly, what's cool is that, over a much longer evolutionary time, they will eventually re-evolve the behavior that has them eating both types of food, but this time with *preference*—they will continue to prefer their original preference for either red or blue, but, in the absence of their preferred food, will eat the other type. It's funny to watch a dot approach its non-preferred food, then, even at just two pixels' distance from it, turn around and make a beeline for its preferred food that just appeared. It's not a completely optimal solution, but pretty fun to watch, and interesting.

What's also rather surprising is that the dots have also learned, all on their own, to head towards the center of the field while waiting for more food to appear. Given the randomness of where food will appear, the middle is obviously the best place to be, but it's fascinating that the selective pressure (with sexual reproduction) is actually enough to produce this behavior.

Speaking of sexual reproduction, it's also really interesting to see the population fitness numbers change as populations evolve. After a beneficial mutation, the average fitness rapidly rises, as the "genes" are quickly distributed across the population via recombination. Here's a fitness graph of one simulation:

![fitness progression](examples/dots-fitness-progression.png "Fitness Progression")

## Have fun!
If you decide to download this, have fun! Feel free to let me know of any interesting results, or, if you would like assistance with using it, just ask!