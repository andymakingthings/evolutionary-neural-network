//
//  avoid-the-walls.cpp
//  ego
//

#include "avoid-the-walls.hpp"
#include "graphics.hpp"
#include <gooey-lib/math.hpp>
#include <cmath>
#include <random>
#include <iostream>
#include <boost/math/constants/constants.hpp>

namespace ego { namespace avoid_the_walls_utility {
class dot : public ego::Rect
{
public:
	int counter {0};
	bool active () {return counter < 1 ? true : false;}
};
}} // namespace ego::avoid_the_walls

// rate an individual in the population:
double avoid_the_walls (Neural_Lattice & ego, int n_turns, ego::SDL_Display_Package sdl_package, bool graphics_on, bool output_on)
{
	const unsigned long long int N_FIRING = gooey::math::fix_within_minimum(ego.cluster.size()-1, 1);
	const int WALL_WIDTH = 12;
	const int ROOM_WIDTH = sdl_package.width-WALL_WIDTH*2;
	const int ROOM_HEIGHT = sdl_package.height-WALL_WIDTH*2;
	const int DUDE_SIZE = 16;
	const int SPEED = 8;
	const int DELAY = 10;
	bool moved_at_least_once = false;
	
	SDL_Rect walls;
		walls.x = 0;
		walls.y = 0;
		walls.w = sdl_package.width;
		walls.h = sdl_package.height;
	SDL_Rect room;
		room.x = (sdl_package.width-ROOM_WIDTH)/2;
		room.y = (sdl_package.height-ROOM_HEIGHT)/2;
		room.w = ROOM_WIDTH;
		room.h = ROOM_HEIGHT;
	
	ego::Rect little_dude;
		little_dude.sdl_rect.x = 0;
		little_dude.sdl_rect.x = 0;
		little_dude.sdl_rect.w = DUDE_SIZE;
		little_dude.sdl_rect.h = DUDE_SIZE;
		little_dude.sdl_color = {255, 220, 200, 255};

	std::vector<ego::avoid_the_walls_utility::dot> little_dots;
		for (int i = 0; i < 2; ++i) little_dots.emplace_back();
		for (auto &dot : little_dots)
		{
			dot.sdl_rect.x = 0;
			dot.sdl_rect.y = 0;
			dot.sdl_rect.w = DUDE_SIZE;
			dot.sdl_rect.h = DUDE_SIZE;
			//dot.sdl_color = {255, 100, 100, 255}
		}
		little_dots[0].sdl_color = {255, 128, 128, 255};
		little_dots[1].sdl_color = {170, 170, 255, 255};
	
	auto update_window = [&]()
	{
		// Clear window background
		SDL_SetRenderDrawColor( sdl_package.renderer, 255, 255, 255, 255 );
		SDL_RenderClear( sdl_package.renderer );
		
		// draw the walls
		SDL_SetRenderDrawColor( sdl_package.renderer, 0, 0, 0, 255 );
		SDL_RenderFillRect( sdl_package.renderer, &walls );
		
		// fill the room
		SDL_SetRenderDrawColor( sdl_package.renderer, 53, 181, 112, 255 );
		SDL_RenderFillRect( sdl_package.renderer, &room );
		
		// draw our little dots
		for (auto &dot : little_dots) if (dot.active()) draw_rectangle(sdl_package.renderer, dot);
		
		// draw our little dude
		draw_rectangle(sdl_package.renderer, little_dude);
		
		// Render to the screen
		SDL_RenderPresent(sdl_package.renderer);
	};
		
	// start at random coordinates
	static std::random_device rd;
	static std::mt19937 choose(rd());
	std::uniform_int_distribution<> random_height(WALL_WIDTH, WALL_WIDTH + ROOM_HEIGHT);
	std::uniform_int_distribution<> random_width(WALL_WIDTH, WALL_WIDTH + ROOM_WIDTH);
	
	double x_new = 0, y_new = 0;
	auto square = [](double x){return x*x;};
	// don't hit the wall!
	double score {0};
	double bonus {1.5}; // if they reach the end without dying
	double dot_bonus{ 1.2};
	float survive {0};
	float minimum_score {.01};
	const int split_game {4};
	for (int c = 0; c < (graphics_on? 1 : split_game); ++c)
	{
		little_dude.sdl_rect.x = random_width(choose);
		little_dude.sdl_rect.y = random_height(choose);
		for (auto &dot : little_dots)
		{
			dot.sdl_rect.x = random_width(choose);
			dot.sdl_rect.y = random_height(choose);
		}
		ego.forget();
		
		for (int i = 0; i < (graphics_on? n_turns : n_turns/split_game); ++i)
		{
			ego.cluster[0].neurons[0]->push_input(little_dude.sdl_rect.x);
			ego.cluster[0].neurons[1]->push_input(little_dude.sdl_rect.y);
			ego.cluster[0].neurons[2]->push_input(ROOM_WIDTH-little_dude.sdl_rect.x);
			ego.cluster[0].neurons[3]->push_input(ROOM_HEIGHT-little_dude.sdl_rect.y);
			int j = 4;
			for (auto &dot : little_dots)
			{
				ego.cluster[0].neurons[j]->push_input(dot.active()? little_dude.sdl_rect.x-dot.sdl_rect.x : 0);
				++j;
				ego.cluster[0].neurons[j]->push_input(dot.active()? little_dude.sdl_rect.y-dot.sdl_rect.y : 0);
				++j;
			}
			
			// get the output
			ego.pulse(N_FIRING);
			double x_pull = ego.cluster.back().neurons[ego.cluster.back().neurons.size()-1]->get_value();
			double y_pull = ego.cluster.back().neurons[ego.cluster.back().neurons.size()-2]->get_value();
			
			double h = std::sqrt(x_pull*x_pull + y_pull*y_pull);
			if (h > SPEED)
			{
				x_pull *= SPEED/h;
				y_pull *= SPEED/h;
				//h = SPEED;
			}
			
			x_new = little_dude.sdl_rect.x + x_pull;
			y_new = little_dude.sdl_rect.y + y_pull;
			
			// check for collision between little_dude and the dot
			for (auto &dot : little_dots)
			{
				--dot.counter;
				if (dot.active() && (std::abs(x_new-dot.sdl_rect.x) < DUDE_SIZE) && (std::abs(y_new-dot.sdl_rect.y) < DUDE_SIZE))
				{
					survive = 1;
					score *= dot_bonus;
					dot.sdl_rect.x = random_width(choose);
					dot.sdl_rect.y = random_height(choose);
					dot.counter = 100;
				}
			}
			
			if(graphics_on)
			{
				update_window ();
				
				// replace this:
				//SDL_Delay(DELAY);
				
				// with this:
				for (int idelay = 0; idelay < DELAY; ++idelay)
				{
					SDL_PumpEvents();
					SDL_Delay(1);
				}
			}
			
			if (x_new > ROOM_WIDTH + WALL_WIDTH || x_new < WALL_WIDTH || y_new > ROOM_HEIGHT + WALL_WIDTH || y_new < WALL_WIDTH)
			{
				return score * survive + minimum_score; // game over!
			}
			else
			{
				// points for distance walked within boundaries
				int distance_traveled = std::pow((square(x_new - little_dude.sdl_rect.x) + square(y_new - little_dude.sdl_rect.y)), 0.5);
				//score += distance_traveled/SPEED/1000;
				if (!moved_at_least_once)
				{
					if (distance_traveled > 0)
					{
						moved_at_least_once = true;
						score += 2;
					}
				}
				// update position
				little_dude.sdl_rect.x = x_new;
				little_dude.sdl_rect.y = y_new;
			}
		}
	}
	
	return score * bonus * survive + minimum_score;
}

