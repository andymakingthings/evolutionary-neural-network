//
//  avoid-the-walls.hpp
//  ego
//

#ifndef __ego__avoid_the_walls__
#define __ego__avoid_the_walls__

#include "neural-lattice.hpp"
#include "graphics.hpp"

double avoid_the_walls (Neural_Lattice & ego, int n_turns, ego::SDL_Display_Package sdl_package, bool graphics_on = false, bool output_on = false);

#endif /* defined(__ego__utility__) */
