//
//  axon.cpp
//  ego
//

#include "axon.hpp"
#include "neuron.hpp"
#include "neural-network.hpp"
#include <gooey-lib/math.hpp>
#include <map>
#include <limits>
#include <cmath>

Neural_Address::Neural_Address(unsigned long long int network_id_init, unsigned long long int neuron_id_init) : network_id(network_id_init), neuron_id(neuron_id_init) {}

bool operator== (Neural_Address const & lhs, Neural_Address const & rhs)
{
	return lhs.neuron_id == rhs.neuron_id && lhs.network_id == rhs.network_id;
}

Axon_Option::Axon_Option(Update update_opt, Skip_Update skip_opt, Prune prune_opt) : update_option(update_opt), skip_update_option(skip_opt), prune_option(prune_opt) {}

Axon::~Axon()
{
	// release old target, if there is one
	if (neuron_target != nullptr)
		neuron_target->receive_message(Message::releasing, Relation::neighbor, this);
	
	// notify parent.
	if (parent_neuron != nullptr)
		parent_neuron->receive_message(Message::releasing, Relation::family, this, neuron_target);
}

Axon::Axon(Axon const & copy_from) : id(copy_from.id), neuron_address(copy_from.neuron_address), location(copy_from.location), weight(copy_from.weight), fixed_weight(copy_from.fixed_weight), fixed_target(copy_from.fixed_target), suppression(copy_from.suppression) {}

unsigned long long int Axon::get_id() const
{
	return id;
}

void Axon::set_id(unsigned long long int x)
{
	id = x;
}

Neuron * const Axon::get_parent_neuron() const
{
	return parent_neuron;
}

void Axon::set_parent_neuron(Neuron * n)
{
	parent_neuron = n;
}

Neural_Address Axon::get_neural_address() const
{
	return neuron_address;
}

bool Axon::set_neural_address(Neural_Address const & address)
{
	// try to get permission from parent neuron
	auto old_address = neuron_address;
	neuron_address = address;
	// ask permission, if denied revert the change:
	if (parent_neuron->receive_message(Message::address_change, Relation::family, this) == Response::permitted)
		return true;
	else
	{
		neuron_address = old_address;
		return false;
	}
}

void Axon::prune_targets()
{
	// ignore this request if target is currently fixed
	if (fixed_target) return;
	
	// release old target, if there is one
	if (neuron_target != nullptr)
		neuron_target->receive_message(Message::releasing, Relation::neighbor, this);
	
	// notify parent
	if (parent_neuron != nullptr)
		parent_neuron->receive_message(Message::releasing, Relation::family, this, neuron_target);
	
	// clear the target pointers
	neuron_target = nullptr;
	network_target = nullptr;
}

bool Axon::change_neural_address(Neural_Address address)
{
	// ignore this request if target is currently fixed, or if the address has
	// not changed.
	if (fixed_target || neuron_address == address) return false;
	
	// release old target (if there is one), since it should be different now.
	prune_targets();
	
	// update the address
	return set_neural_address(address);
}

Axon::Response Axon::receive_message(Message message, Relation relation, Neuron * neuron)
{
	switch (message)
	{
		case Message::address_change:
			
			// family is being weird, no idea
			if (relation == Relation::family)
				return Response::confused;
			
			// a neuron is notifiying its change of address
			else if (relation == Relation::neighbor)
			{
				// message was erroneous or malformed
				if(neuron == nullptr || neuron != neuron_target)
					return Response::confused;
				
				// try to set the new address, if parent allows it. otherwise
				// prune because old address is invalid and new is not permitted
				if ( !set_neural_address( neuron->get_neural_address() ) )
				{
					prune_targets();
					return Response::denied;
				}
				else return Response::permitted;
			}
			break;
			
		case Message::aptosis:
			
			// check to see if the message is erroneous
			if(neuron == nullptr || neuron != neuron_target)
				return Response::confused;
			
			// try to notify parent neuron
			if(neuron != parent_neuron && parent_neuron != nullptr)
				parent_neuron->receive_message(Message::releasing, Relation::neighbor, this, neuron_target);
			
			network_target = nullptr;
			neuron_target = nullptr;
			return Response::permitted;
			break;
			
		case Message::attaching: // senseless message coming from a neuron
		case Message::releasing: // also senseless: return confused
			break;
	}
	return Response::confused;
}

Neural_Address::Location Axon::get_location() const
{
	return location;
}

void Axon::set_location(Neural_Address::Location L)
{
	location = L;
}

Neuron * const Axon::get_target() const
{
	return neuron_target;
}

void Axon::set_target(Neural_Address address, Neuron * target_n, Neural_Network * target_nn)
{
	// ignore this request if target is currently fixed
	if (fixed_target) return;
	
	// detach from old target
	prune_targets();
	
	change_neural_address(address);

	// if the parent neuron is inside a network (and knows it), we can update
	// our location to external or internal:
	if (parent_neuron != nullptr)
	{
		if (parent_neuron->get_parent_network() != nullptr)
		{
			if (target_nn == parent_neuron->get_parent_network())
				location = Neural_Address::Location::internal_address;
			else
				location = Neural_Address::Location::external_address;
		}
	}
	
	// notify parent and target of attachment
	if(target_n != nullptr)
	{
		// request permission to attach from parent neuron (if there is a parent)
		// and, if permission is granted (or no parent to restrict), attach
		if ( (parent_neuron == nullptr) ? true : (parent_neuron->receive_message(Message::attaching, Relation::family, this, target_n)==Response::permitted) )
		{
			network_target = target_nn;
			neuron_target = target_n;
			neuron_target->receive_message(Message::attaching, Relation::neighbor, this);
		}
	}
}

void Axon::update_target(std::map<unsigned long long int, Neural_Network *> & networks, Axon_Option update_options)
{
	// ignore this request if target is currently fixed
	if (fixed_target) return;

	// 1. Axon_Option::Update flag
	switch (update_options.update_option)
	{
		case Axon_Option::Update::every_axon:
			// every_axon: all axons will attempt to update. continue.
			break;
		case Axon_Option::Update::if_external_axon:
			// if_external_axon: only external axons will update. if axon is internal,
			// skip the update (return).
			if (location == Neural_Address::Location::internal_address) return;
			break;
		case Axon_Option::Update::if_internal_axon:
			// if_internal_axon: only internal axons will update. if external, skip.
			if (location == Neural_Address::Location::external_address) return;
			break;
	}
	
	// 2. Find the Neural_Network of this axon's target neuron
	auto it_nn = networks.find(neuron_address.network_id);
	
	// if network is not found in the list, decide whether to prune or not and
	// then return (no neuron to look up).
	if (it_nn == networks.end())
	{
		switch (update_options.prune_option)
		{
			case Axon_Option::Prune::all_unmatched:
				prune_targets();
				break;
			case Axon_Option::Prune::if_network_absent_from_list:
				prune_targets();
				break;
			case Axon_Option::Prune::if_neuron_absent_from_network:
				break;
			case Axon_Option::Prune::none:
				break;
		}
		return;
	}
	
	// network was found in the list.
	Neural_Network * target_nn = (*it_nn).second;
	
	// if it's null, for some weird reason, then it must be skipped (can't update).
	if (target_nn == nullptr) return;
	
	// if it's the same as the current target, then it's safe to skip (return)
	// because neurons would have already updated the axon upon aptosis or
	// change of address.
	if (target_nn == network_target && neuron_target != nullptr) return;
	
	// check to see whether an update would improperly change location
	// (internal/external) based on update options
	
	// (for legibility—compiler should optimize this, i'd imagine.)
	bool currently_internal = (location == Neural_Address::Location::internal_address)? true: false;
	bool currently_external = !currently_internal;
	
	// orphaned means the current axon has no parent network, somehow.
	bool currently_orphaned = true;
	if (parent_neuron != nullptr)
		if (parent_neuron->get_parent_network() != nullptr) currently_orphaned = false;
	
	bool target_is_external, target_is_internal;
	
	if (currently_orphaned)
	{
		target_is_external = false;
		target_is_internal = false;
	}
	else
	{
		target_is_external = (parent_neuron->get_parent_network() != target_nn)? true: false;
		target_is_internal = !target_is_external;
	}
	
	bool makes_external = (currently_internal && target_is_external)? true: false;
	bool makes_internal = (currently_external && target_is_internal)? true: false;
	
	switch (update_options.skip_update_option)
	{
		case Axon_Option::Skip_Update::if_makes_axon_internal_or_makes_external:
			if (makes_external || makes_internal) return;
			break;
		case Axon_Option::Skip_Update::if_makes_axon_external:
			if (makes_external) return;
			break;
		case Axon_Option::Skip_Update::if_makes_axon_internal:
			if (makes_internal) return;
			break;
		case Axon_Option::Skip_Update::none:
			break;
	}
	
	// done checking against improper location changes
	
	// look for neuron target inside neural network based on the id
	Neuron * target_n = target_nn->get_neuron_by_id(neuron_address.neuron_id);
	
	// if neuron is not found, decide whether to prune or not and then return.
	if (target_n == nullptr)
	{
		switch (update_options.prune_option)
		{
			case Axon_Option::Prune::all_unmatched:
				prune_targets();
				break;
			case Axon_Option::Prune::if_network_absent_from_list:
				break;
			case Axon_Option::Prune::if_neuron_absent_from_network:
				prune_targets();
				break;
			case Axon_Option::Prune::none:
				break;
		}
		return;
	}
	
	// neuron was found, perform update
	set_target(Neural_Address(target_nn->get_id(), target_n->get_id()), target_n, target_nn);
}

void Axon::update_target(Neural_Network * network, Axon_Option update_options)
{
	std::map<unsigned long long int, Neural_Network *> networks_list;
	networks_list.emplace(network->get_id(), network);
	
	update_target(networks_list, update_options);
	return;
}

void Axon::update_target(Axon_Option update_options)
{
	Neural_Network * network = nullptr;
	if (parent_neuron != nullptr) network = parent_neuron->get_parent_network();
	if (network == nullptr) return;
	
	std::map<unsigned long long int, Neural_Network *> networks_list;
	networks_list.emplace(network->get_id(), network);
	
	update_target(networks_list, update_options);
	return;
}

bool Axon::target_fixed() const
{
	return fixed_target;
}

void Axon::fix_target(bool b)
{
	fixed_target = b;
}

double Axon::get_weight() const
{
	return weight;
}

void Axon::set_weight(double x)
{
	//  0 < weight < inf
	// ::min() gives the smallest positive float, but the most negative int.
	// this breaks if changed to use int instead of double.
	weight = gooey::math::fix_within_bounds( x,
		std::numeric_limits<double>::min(),
		std::numeric_limits<double>::max());
	//weight = 1;
}

bool Axon::is_suppressor() const
{
	return suppression;
}

void Axon::set_suppression(bool b)
{
	suppression = b;
}

bool Axon::weight_fixed() const
{
	return fixed_weight;
}

void Axon::fix_weight(bool b)
{
	fixed_weight = b;
}

void Axon::fire(double signal) const
{
	if (neuron_target == nullptr) return;
	
	// push the signal based on value down the pipe of each axon.
	//  -inf < signal*weight < inf and 0 < weight < inf
	// ::min() gives the smallest positive float, but the most negative int.
	// ::lowest() gives the most negative value for both float and int.
	// this should not break if changing to use ints.
	
	double fire_signal = gooey::math::fix_within_bounds (
		signal * (is_suppressor() ? -weight : weight),
		std::numeric_limits<double>::lowest(),
		std::numeric_limits<double>::max());
	
	neuron_target->receive_signal( fire_signal );
}

std::string Neural_Address::to_json()
{
	std::string json = "{";
	json = json
		+ "\"network_id\":\"" + gooey::io::serialize(network_id) + "\","
		+ "\"neuron_id\":\"" + gooey::io::serialize(neuron_id) + "\""
		+ "}";
	return json;
}

std::string Neural_Address::serialize(Neural_Address::Location t) {
	std::string json = "";
	switch (t)
	{
		default:
		case Neural_Address::Location::internal_address:
			json += "internal_address";
			break;
		case Neural_Address::Location::external_address:
			json += "external_address";
			break;
	}
	return json;
}

Neural_Address::Neural_Address(nlohmann::json const & json) {
	network_id = std::stoull((std::string)json["network_id"]);
	neuron_id = std::stoull((std::string)json["neuron_id"]);
}

Neural_Address::Location Neural_Address::Location_deserialize(std::string const & s) {
	if (s == "internal_address") return Neural_Address::Location::internal_address;
	else if (s == "external_address") return Neural_Address::Location::external_address;
	return Neural_Address::Location::internal_address;
}

std::string Axon::to_json()
{
	std::string json = "{";
	json = json
	+ "\"id\":\"" + gooey::io::serialize(id) + "\","
	+ "\"fixed_weight\":\"" + gooey::io::serialize(fixed_weight) + "\","
	+ "\"fixed_target\":\"" + gooey::io::serialize(fixed_target) + "\","
	+ "\"suppression\":\"" + gooey::io::serialize(suppression) + "\","
	+ "\"weight\":\"" + gooey::io::serialize(weight) + "\","
	+ "\"neuron_address\":" + neuron_address.to_json() + ","
	+ "\"location\":\"" + Neural_Address::serialize(location) + "\""
	+ "}";
	return json;
}

Axon::Axon(nlohmann::json const & json) {
	id = std::stoull((std::string)json["id"]);
	fixed_weight = json["fixed_weight"] == "true";
	fixed_target = json["fixed_target"] == "true";
	suppression = json["suppression"] == "true";
	weight = std::stod((std::string)json["weight"]);
	neuron_address = Neural_Address(json["neuron_address"]);
	location = Neural_Address::Location_deserialize(json["location"]);
}
