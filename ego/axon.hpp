//
//  axon.hpp
//  ego
//

#ifndef __ego__axon__
#define __ego__axon__

#include <map>
#include <string>
#include <nlohmann/json.hpp>

class Neural_Address
{
public:
	enum class Location {internal_address, external_address};
	
	unsigned long long int network_id;
	unsigned long long int neuron_id;
	
	Neural_Address(unsigned long long int network_id, unsigned long long int neuron_id);
	Neural_Address(Neural_Address const &) = default;
	Neural_Address() = default;
	
	std::string to_json();
	Neural_Address(nlohmann::json const &);
	static std::string serialize(Location);
	static Location Location_deserialize(std::string const &);
};
bool operator== (Neural_Address const &, Neural_Address const&);
// specializing std::less to use Neural_Address as std::map key
namespace std
{
	template<>
	struct less<Neural_Address>
	{
		bool operator() (Neural_Address const & lhs, Neural_Address const & rhs) const
		{
			return lhs.network_id < rhs.network_id || (lhs.neuron_id < rhs.neuron_id && !(lhs.network_id > rhs.network_id));
		}
	};
}

class Axon_Option
{
public:
	// add option: if pruned axon?
	enum class Update
	{
		every_axon,
		if_external_axon,
		if_internal_axon
	};
	
	enum class Skip_Update
	{
		none,
		if_makes_axon_internal,
		if_makes_axon_external,
		if_makes_axon_internal_or_makes_external
	};
	
	enum class Prune
	{
		none,
		if_neuron_absent_from_network,
		if_network_absent_from_list,
		all_unmatched
	};
	
	Update update_option { Update::every_axon };
	Skip_Update skip_update_option { Skip_Update::none };
	Prune prune_option { Prune::if_neuron_absent_from_network };
	
	Axon_Option(Update, Skip_Update skip_option = Skip_Update::none, Prune prune_option = Prune::if_neuron_absent_from_network);
	Axon_Option() = default;
};


class Neuron;
class Neural_Network;
class Axon
{
private:
	unsigned long long int id{0};
	
	Neuron * parent_neuron{nullptr};
	Neural_Address neuron_address;
	Neural_Address::Location location {Neural_Address::Location::internal_address};
	Neuron * neuron_target{nullptr};
	Neural_Network * network_target{nullptr};
	
	// amplifies/scales signal: ≥ 0
	double weight{1};
	
	// flips signal to negative
	bool suppression{false};
	
	// when fixed, refuse to change weight
	bool fixed_weight{false};
	
	// when fixed, refuse to honor set_target, set_neural_address,
	// and update_target requests. axon will still honor messages
	// of aptosis and change of address from targeted neurons.
	bool fixed_target{false};
	
public:
	virtual ~Axon();
	Axon() = default;
	Axon(Axon const &);
	
	enum class Relation
	{
		family,
		neighbor
	};
	
	enum class Message
	{
		attaching,
		releasing,
		address_change,
		aptosis
	};
	
	enum class Response
	{
		denied,
		permitted,
		confused
	};

	unsigned long long int get_id() const;
	void set_id(unsigned long long int);
	
	Neuron * const get_parent_neuron() const;
	void set_parent_neuron(Neuron *);
	
	void prune_targets();
	
	Neural_Address get_neural_address() const;
	// this sets the address and sends a message to the parent neuron
	bool set_neural_address(Neural_Address const &);
	// this is called when the address is to change, causing axon to prune
	bool change_neural_address(Neural_Address);
	Response receive_message(Message message, Relation relation, Neuron * signature);
	
	Neural_Address::Location get_location() const;
	void set_location(Neural_Address::Location);
	
	Neuron * const get_target() const;
	void set_target(Neural_Address, Neuron *, Neural_Network *);
	
	void update_target(std::map<unsigned long long int, Neural_Network *> & networks, Axon_Option options = Axon_Option());
	
	void update_target(Neural_Network *, Axon_Option update_options = Axon_Option());
	void update_target(Axon_Option update_options = Axon_Option());
	
	bool target_fixed() const;
	void fix_target(bool);
	
	double get_weight() const;
	void set_weight(double);
	
	bool is_suppressor() const;
	void set_suppression(bool);
	
	bool weight_fixed() const;
	void fix_weight(bool);
	
public:
	void fire(double signal) const;
	
	std::string to_json();
	Axon(nlohmann::json const &);
};

//bool operator< (Axon const & lhs, Axon const & rhs);


#endif /* defined(__ego__axon__) */
