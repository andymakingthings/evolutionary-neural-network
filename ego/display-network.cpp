//
//  display-network.cpp
//  ego
//

#include "display-network.hpp"
#include "neural-network.hpp"
#include "graphics.hpp"
#include <vector>
#include <cmath>
#include <iostream>

namespace ego{
class Graphical_Network_Node
{
public:
	SDL_Point point;
	Neuron * neuron;
};

} // namespace ego


void display_network (Neural_Lattice const & network, ego::SDL_Display_Package sdl_package)
{
	std::vector<ego::Graphical_Network_Node> nodes;
	
	// draw each neuron
	const SDL_Point origin {sdl_package.width/2, sdl_package.height/2};
	const double min_wheel_radius = 100;
	const double min_radius = 50;
	const int node_radius = 5;
	const int gap = 40;
	unsigned long long int largest_cluster = 1;
	for (auto &c : network.cluster) if (c.neurons.size() > largest_cluster) largest_cluster = c.neurons.size();
	double largest_cluster_circumference = largest_cluster * (node_radius * 2 + gap);
	// C = 2*Pi*r C/(2Pi) = r find the largest cluster's radius
	double largest_cluster_radius = largest_cluster_circumference/(2*3.15);
	double wheel_circumference = network.cluster.size()*(largest_cluster_radius * 2 + gap);
	double wheel_radius = std::max(wheel_circumference/(2*3.15), min_wheel_radius);
	double wheel_angle_increment = 2*3.15/network.cluster.size();
	double wheel_angle = 0;
	
	for (auto & c : network.cluster)
	{
		double angle_increment = 2*3.15/c.neurons.size();
		double circumference = c.neurons.size()*(node_radius * 2 + gap);
		double radius = std::max(circumference/(2*3.15), min_radius);
		double angle = wheel_angle;
		for (auto & n : c.neurons)
		{
			nodes.emplace_back();
			nodes.back().neuron = n.get();
			
			double x = origin.x, y = origin.y;
			y += wheel_radius*std::sin(wheel_angle);
			x += wheel_radius*std::cos(wheel_angle);
			y += radius*std::sin(angle);
			x += radius*std::cos(angle);
			
			nodes.back().point = {(int)x, (int)y};
			angle += angle_increment;
		}
		
		wheel_angle += wheel_angle_increment;
	}

	// Clear window background
	SDL_SetRenderDrawColor( sdl_package.renderer, 255, 255, 255, 255 );
	SDL_RenderClear( sdl_package.renderer );

	SDL_Color node_color {122, 230, 125, 255};
	SDL_Color line_color_1 {222, 250, 240, 192};
	SDL_Color line_color_2 {222, 233, 250, 128};
	for (auto &n : nodes)
	{
		draw_circle (sdl_package.renderer, n.point.x, n.point.y, node_radius, node_color, 1000);
		// draw line from node to other nodes
		for (auto &a : n.neuron->axons())
		{
			// get target
			auto t = a->get_target();
			if (t == nullptr) { std::cout << "error: target is nullptr\n"; continue; }
			// find target in nodes list
			auto t_node = nodes.begin();
			bool node_found = false;
			for (t_node = nodes.begin(); t_node != nodes.end(); ++t_node)
			{
				if ((*t_node).neuron == t)
				{
					node_found = true;
					break;
				}
			}
			if (!node_found) { std::cout << "error: node not found\n"; continue; }
			
			SDL_Color line_color = (*t_node).neuron->get_parent_network() == n.neuron->get_parent_network()? line_color_1 : line_color_2;
			
			// draw line from n to *t_node
			SDL_SetRenderDrawColor ( sdl_package.renderer, line_color.r, line_color.g, line_color.b, line_color.a );
			SDL_RenderDrawLine(sdl_package.renderer, n.point.x, n.point.y, (*t_node).point.x, (*t_node).point.y);
		}
	}

	// Render to the screen
	SDL_RenderPresent(sdl_package.renderer);

	return;

}

