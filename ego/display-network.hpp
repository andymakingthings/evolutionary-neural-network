//
//  display-network.hpp
//  ego
//

#ifndef __ego__display_network__
#define __ego__display_network__

#include "neural-lattice.hpp"
#include "graphics.hpp"

void display_network (Neural_Lattice const &, ego::SDL_Display_Package);

#endif /* defined(__ego__display_network__) */
