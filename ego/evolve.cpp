//
//  main-c.cpp
//  ego
//

#include <iostream>
#include <list>
#include <random>
#include <gooey-lib/lineage.hpp>
#include <gooey-lib/misc.hpp>
#include <thread>
#include <chrono>
#include "evolve.hpp"
#include "neural-network.hpp"
#include "avoid-the-walls.hpp"
#include "fit-curve.hpp"
#include "organism.hpp"
#include "display-network.hpp"
#include "graphics.hpp"
#include "get-input.hpp"
#include "game-type.hpp"

const int num_threads = 4;

void evolve()
{
	int cluster_count, cluster_size, equilibrium_population_size, population_size, training_time, generations_to_equilibrium;
	bool merge_output_input_layers, wire_internally;
	Game_Type game_type;
	
	if (!get_input(cluster_count, cluster_size, equilibrium_population_size, training_time, game_type, merge_output_input_layers, wire_internally, population_size, generations_to_equilibrium)) return;
	int pop_decrease_each_gen = (population_size - equilibrium_population_size)/generations_to_equilibrium;
	
	auto game = ego::SDL_Game_Package(game_type);
	game.initialize_sdl_windows();
	
	int N_Turns_eval = 2000;
	int N_Turns_display = 1000;
	const int N_Tests = 400;
	//const double xmin = -300, xmax = 300, xmin_display = -50, xmax_display = 50;
	const double xmin = -100, xmax = 100, xmin_display = -20, xmax_display = 20;
	//auto square = [] (double x) { return x*x; };
	//auto curve = [] (double x) { return 17; }; // works well
	//auto curve = [] (double x) { return x; }; // works well
	//auto curve = [] (double x) { return x*x; }; // works well
	//auto curve = [] (double x) { return x*x*x; }; // works well
	//auto curve = [] (double x) { return std::sin(x); }; // works well
	//auto curve = [square] (double x) { return square(std::sin(x)); }; // works well 2, 3, 16k
	//auto curve = [] (double x) { return std::sqrt(x); }; // works well
	//auto curve = [] (double x) { return 5 * x*x + 17; }; // works well
	auto curve = [] (double x) { return 5 * x*x*x + 2 * x*x + 7 * x + 17; }; // works well
	//auto curve = [] (double x) { return 1/x; }; // works well
	//auto curve = [] (double x) { return std::sin(x)/x; };
	auto get_fitness = [&game_type, &N_Turns_eval, &N_Tests, game, &curve, xmin, xmax] (Neural_Lattice &ego)
	{
		switch (game_type)
		{
			case Game_Type::curve:
				return fit_curve(ego, curve, xmin, xmax, N_Tests);
			case Game_Type::dots:
				return avoid_the_walls(ego, N_Turns_eval, game.sdl_game_activity);
			default:
				return 0.0;
		}
	};
	
	auto play_game = [game, &game_type, &N_Turns_display, &N_Tests, &curve, xmin_display, xmax_display] (Neural_Lattice & ego, bool output_on)
	{
		display_network(ego, game.sdl_neural_activity);
		
		switch (game_type)
		{
			case Game_Type::curve:
				return fit_curve(ego, curve, xmin_display, xmax_display, N_Tests, true, game.sdl_game_activity, output_on, true);
			case Game_Type::dots:
				return avoid_the_walls(ego, N_Turns_display, game.sdl_game_activity, true, output_on);
			default:
				return 0.0;
		}
	};
	
	std::list<Animal> ego;
	for (int i = 0; i < population_size; ++i) ego.emplace_back(Animal());
	
	int input_neurons_count = 1, output_neurons_count = 1;
	switch (game_type)
	{
		case Game_Type::curve:
			input_neurons_count = 1;
			output_neurons_count = 1;
			break;
		case Game_Type::dots:
			input_neurons_count = 8;
			output_neurons_count = 2;
			break;
		default:
			break;
	}
	
	// add neural networks
	for (auto & e : ego)
	{
		int id = 0;
		// add input cluster
		e.brain.cluster.emplace_back();
		e.brain.cluster.back().set_id(id++);
		e.brain.cluster.back().grow_neuron(input_neurons_count + (cluster_count > 0 ? 0 : cluster_size) + (merge_output_input_layers ? output_neurons_count : 0));
		
		// add user clusters
		for (int i = 0; i < cluster_count; ++i)
		{
			e.brain.cluster.emplace_back();
			e.brain.cluster.back().set_id(id++);
			e.brain.cluster.back().grow_neuron(cluster_size);
		}
		
		// add output cluster
		if (!merge_output_input_layers)
		{
			e.brain.cluster.emplace_back();
			e.brain.cluster.back().set_id(id++);
			e.brain.cluster.back().grow_neuron(output_neurons_count);
		}
		
		// wire neurons forward
		for (auto c = e.brain.cluster.begin(); c != e.brain.cluster.end(); ++c) if (c+1 != e.brain.cluster.end())
			for (auto & n : (*c).neurons) for (auto & n_f : (*(c+1)).neurons) n->grow_axon(n_f.get());
		
		// each user cluster is internally wired as well
		if (wire_internally)
		{
			bool exclude = cluster_count < 1 ? false : true; // exclude input & output layers unless there are no user clusters
			for (int i = exclude? 1 : 0; i < e.brain.cluster.size() - (exclude? 1 : 0); ++i)
				for (auto & n : e.brain.cluster[i].neurons)
					for (auto & n_t : e.brain.cluster[i].neurons) n->grow_axon(n_t.get());
		}
	}
	
	{
		std::thread threads[num_threads];
		auto randomize = [&ego](unsigned long i_begin, unsigned long n)
		{
			auto e = ego.begin();
			for (int i = 0; i < i_begin; ++i) ++e;
			for (int i = 0; i < n; ++i)
			{
				if (e == ego.end()) break;
				for (auto &c : (*e).brain.cluster) c.randomize(5); // (sigma)
				++e;
			}
		};
		
		// randomize them
		unsigned long n = ego.size()/num_threads+1;
		for (int i = 0; i < num_threads; ++i)
		{
			threads[i] = std::thread(randomize, i*n, n);
			//randomize(i*n, n);
			//for (auto & e : ego) for (auto & c : e.brain.cluster) c.randomize(5); // (sigma)
		}
		for (auto &t : threads) t.join();
	}
	
	// some variables to be consistent:
	float N_Fitness_Fraction = 0.5;
	
	std::random_device rd;
	std::mt19937 choose(rd());
	
	// in a separate thread, display the fittest in action
	std::thread display_thread;
	std::vector<Animal> fittest_display;
	std::vector<Animal*> new_fittest_display; // new fittest list for update
	bool fittest_display_halt = false; // halts the thread
	std::mutex new_fittest;
	
	std::thread training_thread;
	auto training_thread_fn = [&training_time, &ego, &get_fitness, &new_fittest, &fittest_display, &equilibrium_population_size, &pop_decrease_each_gen, &population_size, &N_Fitness_Fraction, &fittest_display_halt] ()
	{
		for (int g = 0; g < training_time; ++g)
		{
			std::cout << std::round((float)g/training_time*100*10)/10 << "% "<< g;
			std::cout << " population size: " << ego.size();
			
			// rate everyone's fitness/error initially
			{
				std::thread threads[num_threads];
				auto rate_fitness = [&ego, &get_fitness](unsigned long i_begin, unsigned long n)
				{
					auto e = ego.begin();
					for (int i = 0; i < i_begin; ++i) ++e;
					
					for (int i = 0; i < n; ++i)
					{
						if (e == ego.end()) break;
						
						//for (auto &e : ego) e.fitness = get_fitness(e.brain);
						(*e).fitness = get_fitness((*e).brain);
						
						++e;
					}
				};
				
				unsigned long n = ego.size()/num_threads+1;
				for (int i = 0; i < num_threads; ++i)
					threads[i] = std::thread(rate_fitness, i*n, n);
				
				for (auto &t : threads) t.join();
			}
			
			/*// get the average fitness
			double average_fitness{0};
			for (auto &e : ego) average_fitness += e.fitness;
			average_fitness /= population_size;
			std::cout << " \taverage fitness : " << average_fitness;*/
			
			// get the median fitness
			double median_fitness{0};
			{
				std::list<double> fitnesses;
				for (auto &e : ego) fitnesses.emplace_back(e.fitness);
				fitnesses.sort();
				
				int j = 0;
				for (auto f = fitnesses.begin(); f != fitnesses.end(); ++f)
				{
					if (j == fitnesses.size()/2)
					{
						median_fitness = *f;
						break;
					}
					++j;
				}
			}
			
			std::cout << " \tmedian fitness : " << median_fitness;
			
			// get the top fitness
			Animal * fittest = &ego.front();
			for (auto &e : ego) if (fittest->fitness < e.fitness) fittest = &e;
			std::cout	<< " \ttop fitness : " << fittest->fitness << std::endl;
			
			// display the fittest in action
			{
				std::lock_guard<std::mutex> guard(new_fittest);
				fittest_display.clear();
				fittest_display.push_back(*fittest);
			}
			
			// grow the population into ego_next and splice to ego
			std::list<Animal> ego_next;
			
			if (population_size > equilibrium_population_size) population_size =
				gooey::math::fix_within_minimum(population_size - pop_decrease_each_gen, equilibrium_population_size);
			// winners get to mate extra, and mutate less
			// first get a list of winners
			auto mating_winners = gooey::lineage::stochastic_universal_sampling( ego,
				[](Animal e){return e.fitness;},
				N_Fitness_Fraction*population_size);

			std::list<Animal> ego_next_multithread[num_threads];
			// winners mate
			{
				std::thread threads[num_threads];
				auto mate = [&mating_winners, &ego](unsigned long i_begin, unsigned long n, std::list<Animal> * ego_next_mt)
				{
					auto female = mating_winners.begin();
					for (int i = 0; i < i_begin; ++i) ++female;
					
					for (int i = 0; i < n; ++i)
					{
						if (female == mating_winners.end()) break;
						
						auto male = gooey::lineage::random_selection(ego);
						ego_next_mt->emplace_back(Animal(**female, *male));
						ego_next_mt->back().brain.mutate();
						
						++female;
					}
				};
				
				unsigned long n = mating_winners.size()/num_threads+1;
				for (int i = 0; i < num_threads; ++i)
					threads[i] = std::thread(mate, i*n, n, &ego_next_multithread[i]);
				
				for (auto &t : threads) t.join();
				
				for (int i = 0; i < num_threads; ++i)
					ego_next.splice(ego_next.end(), ego_next_multithread[i]);
				
				// losers mate to fill the remainder of the next generation
				// first get a list of new winners
				mating_winners.clear();
				mating_winners = gooey::lineage::stochastic_universal_sampling( ego,
					[](Animal e){return 1;},
					population_size - ego_next.size());
				
				// losers mate
				
				n = mating_winners.size()/num_threads+1;
				for (int i = 0; i < num_threads; ++i)
					threads[i] = std::thread(mate, i*n, n, &ego_next_multithread[i]);
				
				for (auto &t : threads) t.join();
				
				for (int i = 0; i < num_threads; ++i)
					ego_next.splice(ego_next.end(), ego_next_multithread[i]);
			}
			ego.swap(ego_next); // old generation dies!!!
		}
		fittest_display_halt = true;
	};
	
	training_thread = std::thread(training_thread_fn);
	int filename_i = 0;
	while(!fittest_display_halt)
	{
		while (!fittest_display_halt && fittest_display.size() == 0)
		{
			if (fittest_display_halt) break;
			std::this_thread::sleep_for(std::chrono::milliseconds(10));
		}
		Animal * a = nullptr;
		{
			std::lock_guard<std::mutex> guard(new_fittest);
			if (fittest_display.size() > 0) a = new Animal(fittest_display.back());
		}
		if (a != nullptr)
		{
			play_game(a->brain, false);
			std::string filename = "best-" + std::to_string(filename_i) + ".json";
			gooey::misc::belch_textfile(filename, a->to_json());
			++filename_i;
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}
	
	if (training_thread.joinable()) training_thread.join();
	std::cout << " ...done!\n\n";
	
	
	// rate everyone's fitness/error initially
	{
		std::thread threads[num_threads];
		auto rate_fitness = [&ego, &get_fitness](unsigned long i_begin, unsigned long n)
		{
			auto e = ego.begin();
			for (int i = 0; i < i_begin; ++i) ++e;
			
			for (int i = 0; i < n; ++i)
			{
				if (e == ego.end()) break;
				(*e).fitness = get_fitness((*e).brain);
				++e;
			}
		};
		
		unsigned long n = ego.size()/num_threads+1;
		for (int i = 0; i < num_threads; ++i)
			threads[i] = std::thread(rate_fitness, i*n, n);
		
		for (auto &t : threads) t.join();
		
		int i = 0;
		for (auto &e : ego)
			std::cout << "ego " << i++ << ": fitness = " << e.fitness << "\n";
	}
	
	do
	{
		std::cout << "\nchoose an ego (0 to exit): ";
		int i{0};
		do
		{
			std::cin >> i;
			std::cout << " ";
		}
		while(i < 0 || i > ego.size());
		if(i == 0) break;
		
		auto e = ego.begin();
		for (int j = 0; j < i-1; ++j) ++e;
		
		std::cout << play_game((*e).brain, true);
	}
	while(true);

	std::cout << "\nbye! :)" << std::endl;
}
