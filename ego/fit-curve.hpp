//
//  fit-curve.hpp
//  ego
//

#ifndef __ego__fit_curve__
#define __ego__fit_curve__

#include "neural-network.hpp"
#include "graphics.hpp"
#include <gooey-lib/math.hpp>
#include <cmath>
#include <random>
#include <iostream>
#include <vector>

// rate an individual in the population:
template <typename Fn>
double fit_curve (Neural_Lattice & ego, Fn function, int X_MIN_, int X_MAX_, int n_tests, bool graphics_on = false, ego::SDL_Display_Package sdl_package = ego::SDL_Display_Package(), bool output_on = false, bool connect_curves = false)
{
	const unsigned long long int N_FIRING = gooey::math::fix_within_minimum(ego.cluster.size()-1, 1);
	
	const int DOT_DIAMETER = 2;
	
	std::vector<ego::Rect> guess_dots;
	std::vector<ego::Rect> correct_dots;
	
	auto update_window = [&]()
	{
		SDL_SetRenderDrawBlendMode(sdl_package.renderer, SDL_BLENDMODE_BLEND);
		
		// Clear window background
		SDL_SetRenderDrawColor ( sdl_package.renderer, 255, 255, 255, 255 );
		SDL_RenderClear( sdl_package.renderer );
		
		// draw our guess dots
		for (auto &dot : guess_dots) draw_rectangle(sdl_package.renderer, dot);
		
		// draw our correct dots
		for (auto &dot : correct_dots) draw_rectangle(sdl_package.renderer, dot);
		
		if (connect_curves)
		{
			// draw lines connecting dots (guess)
			for (auto dot_it = guess_dots.begin(), dot_prev = guess_dots.begin(); dot_it != guess_dots.end(); ++dot_it)
			{
				if (dot_it == guess_dots.begin()) continue;
				
				SDL_SetRenderDrawColor ( sdl_package.renderer, (*dot_prev).sdl_color.r, (*dot_prev).sdl_color.g, (*dot_prev).sdl_color.b, (*dot_prev).sdl_color.a/4 );
				SDL_RenderDrawLine(sdl_package.renderer, (*dot_prev).sdl_rect.x, (*dot_prev).sdl_rect.y, (*dot_it).sdl_rect.x, (*dot_it).sdl_rect.y);
				
				++dot_prev;
			}
			
			// draw lines connecting dots (correct)
			for (auto dot_it = correct_dots.begin(), dot_prev = correct_dots.begin(); dot_it != correct_dots.end(); ++dot_it)
			{
				if (dot_it == correct_dots.begin()) continue;
				
				SDL_SetRenderDrawColor ( sdl_package.renderer, (*dot_prev).sdl_color.r, (*dot_prev).sdl_color.g, (*dot_prev).sdl_color.b, (*dot_prev).sdl_color.a/4 );
				SDL_RenderDrawLine(sdl_package.renderer, (*dot_prev).sdl_rect.x, (*dot_prev).sdl_rect.y, (*dot_it).sdl_rect.x, (*dot_it).sdl_rect.y);
				
				++dot_prev;
			}
		}
		
		// Render to the screen
		SDL_RenderPresent(sdl_package.renderer);
	};

	// start at random coordinates
	static std::random_device rd;
	static std::mt19937 choose(rd());
	
	std::uniform_real_distribution<> random_float(X_MIN_, X_MAX_);
	
	// x, y_ideal, y_guess, error1, error2
	std::map<double, std::tuple<double, double, double, double>> tests;
	
	const double dx = std::numeric_limits<double>::min() * 10000;
	for (int i = 0; i < n_tests; ++i)
	{
		const double x = random_float(choose);
		tests.emplace(x, std::make_tuple(0, 0, 0, 0));
		tests.emplace(x + dx, std::make_tuple(0, 0, 0, 0));
	}
	
	for (auto &t : tests)
	{
		ego.forget();
		ego.cluster.front().neurons[0]->push_input(t.first);
		ego.pulse(N_FIRING);
		
		const double y_guess = ego.cluster.back().neurons[0]->get_value();
		
		t.second = std::make_tuple(function(t.first), y_guess, 0, 0);
	}
	
	// do linear regression: f = y, g = y_guess
	long int tests_size = tests.size();
	long double gf_mean = 0;
	for (auto &t : tests) gf_mean += std::get<1>(t.second) * std::get<0>(t.second) / tests_size;
	
	long double g_mean = 0;
	for (auto &t : tests) g_mean += std::get<1>(t.second) / tests_size;
	
	long double f_mean = 0;
	for (auto &t : tests) f_mean += std::get<0>(t.second) / tests_size;
	
	long double g_squared_mean = 0;
	for (auto &t : tests) g_squared_mean += std::get<1>(t.second)*std::get<1>(t.second) / tests_size;
	
	long double Beta_hat = (gf_mean - g_mean * f_mean) / ( g_squared_mean - g_mean * g_mean );
	long double Alpha_hat = f_mean - Beta_hat * g_mean;
	
	// x, y_ideal, y_guess, error1, error2
	std::map<double, std::tuple<double, double, double, double>> tests_tmp = tests;
	tests.clear();
	for (auto &t : tests_tmp)
	{
		tests.emplace(t.first, std::make_tuple(std::get<0>(t.second), (Beta_hat * std::get<1>(t.second) + Alpha_hat), 0, 0));
		if (!std::isfinite(std::get<1>(t.second)))
		{
			tests.clear();
			tests = tests_tmp;
			break;
		}
	}
	
	// get error
	{
	 double x_prev = (*tests.begin()).first;
	 double y_prev = 0;
	 double y_guess_prev = 0;
	for (auto &t : tests)
	{
		// error1 is ratio f(x)/guess(x) - 1
		double gx = (std::get<1>(t.second) != 0 ? std::get<1>(t.second) : std::numeric_limits<double>::min());
		std::get<2>(t.second) = std::abs(std::get<0>(t.second) / gx - 1);
		
		// error 2 is ratio f'(x) / guess'(x) - 1 : (y-y_prev) / (x-x_prev)
		if (x_prev != t.first)
		{
			double dx = (t.first - x_prev != 0 ? t.first - x_prev : std::numeric_limits<double>::min());
			double f_deriv = (std::get<0>(t.second)-y_prev)/dx;
			double g_deriv = (std::get<1>(t.second)-y_guess_prev)/dx;
			std::get<3>(t.second) = f_deriv != g_deriv
				? std::abs( (g_deriv == 0 ? 0 : f_deriv / g_deriv) - 1)
				: 0;
		}
		else std::get<3>(t.second) = std::get<2>(t.second);
		x_prev = t.first;
		y_prev = std::get<0>(t.second);
		y_guess_prev = std::get<1>(t.second);
	}}
	// scale up errors - scores are too high
	for (auto &t : tests)
	{
		std::get<2>(t.second) *= 10;
		std::get<3>(t.second) *= 10;
	}
	
	double Y_MIN_ = 0, Y_MAX_ = 0;
	for (auto &t : tests)
	{
		if (Y_MAX_ < std::get<0>(t.second)) Y_MAX_ = std::get<0>(t.second);
		if (Y_MIN_ > std::get<0>(t.second)) Y_MIN_ = std::get<0>(t.second);
	}
	
	const double X_SHIFT = -X_MIN_;
	const double Y_SHIFT = Y_MIN_ == Y_MAX_ ? -Y_MIN_ + sdl_package.height/2 : -Y_MIN_;
	const double X_SCALE = (double) sdl_package.width  / ( X_MAX_ - X_MIN_ );
	const double Y_SCALE = Y_MAX_ == Y_MIN_ ? 1 : (double) sdl_package.height / ( Y_MAX_ - Y_MIN_ );
	
	double score_fn {0};
	double score_deriv {0};
	double score {0};
	
	for (auto &t : tests)
	{
		double error_1 = std::get<2>(t.second);
		score_fn += error_1*error_1;
		double error_2 = std::get<3>(t.second);
		score_deriv += error_2*error_2;
	}
	score_fn = gooey::math::fix_within_maximum(score_fn, std::numeric_limits<double>::max());
	score_deriv = gooey::math::fix_within_maximum(score_deriv, std::numeric_limits<double>::max());
	score = score_fn + score_deriv;
	
	if(graphics_on)
	{
		for (auto &t : tests)
		{
			correct_dots.emplace_back();
			guess_dots.emplace_back();
			
			correct_dots.back().sdl_rect.w = DOT_DIAMETER;
			correct_dots.back().sdl_rect.h = DOT_DIAMETER;
			guess_dots.back().sdl_rect.w = DOT_DIAMETER;
			guess_dots.back().sdl_rect.h = DOT_DIAMETER;
			
			correct_dots.back().sdl_color = { 0, 255, 100, 128 };
			guess_dots.back().sdl_color = { 255, 0, 100, 128 };
			
			correct_dots.back().sdl_rect.x = (t.first + X_SHIFT) * X_SCALE;
			guess_dots.back().sdl_rect.x = correct_dots.back().sdl_rect.x;
			
			correct_dots.back().sdl_rect.y = sdl_package.height-(std::get<0>(t.second) + Y_SHIFT) * Y_SCALE;
			guess_dots.back().sdl_rect.y = sdl_package.height-(std::get<1>(t.second) + Y_SHIFT) * Y_SCALE;
		}
		update_window ();
	}
	
	if (output_on) for (auto &t : tests)
	{
		 std::cout
			<< " x: " << t.first
			<< " f(x): " << std::get<0>(t.second)
			<< " guess: " << std::get<1>(t.second)
			<< " error (f(x)): " << std::get<2>(t.second)
			<< " error (f'(x)): " << std::get<3>(t.second)
			<< " score: " << score
			<< "\n";
	}
	
	return 1000*1/(1+score/(double)n_tests);
}

#endif /* defined(__ego__polynomial__) */
