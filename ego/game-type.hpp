
//
//  game-type.h
//  ego
//
//  Created by Andy Carlos on 8/10/19.
//  Copyright © 2019 Andy Carlos. All rights reserved.
//

#ifndef game_type_h
#define game_type_h

enum class Game_Type
{
	dots,
	curve,
	count
};


#endif /* game_type_h */
