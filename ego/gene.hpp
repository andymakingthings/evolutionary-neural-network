//
//  gene.hpp
//  ego
//

#ifndef __ego__gene__
#define __ego__gene__

#include <gooey-lib/lineage.hpp>
#include <random>
#include <array>

int count_parameters() { return 0; } // terminus
template <typename Head, typename ... Tail>
int count_parameters (Head head, Tail... tail)
{
	return 1 + count_parameters (&tail...);
}

template <typename Head, typename ... Tail>
Head select_parameter (int i, Head & head, Tail & ...tail)
{
	if (i == 0) return head;
	else return select_parameter (--i, &tail...);
}

template <typename Head, typename ... Tail>
Head select_random_parameter (std::mt19937 & choose, Head head, Tail... tail)
{
	int i = count_parameters (&tail...);
	std::uniform_int_distribution<> selection (0, i-1);
	return select_parameter( selection(choose), head, &tail... );
}

template <typename Head, typename ... Tail>
Head select_random_parameter (Head head, Tail... tail)
{
	std::random_device rd;
	std::mt19937 choose(rd());
	return select_random_parameter (choose, head, &tail...);
}


/*
enum class Nucleotide { A, T, C, G };

using Codon = std::array<Nucleotide, 3>;

using Genome = std::vector<Codon>;

class Neural_Network_Genome
{
public:
	Genome codons;
	
	virtual ~Allele() = default;
	virtual void mutate(double sigma) = 0;
	enum class Variants;
	Variants variant;
	gooey::lineage::Mutable_Range value;
	gooey::lineage::Mutable_Range mutation_rate;
	void select_random_variant();
	
	//proteins code for behavior.
	
};

void Allele::select_random_variant()
{
	
}
*/
#endif /* defined(__ego__gene__) */
