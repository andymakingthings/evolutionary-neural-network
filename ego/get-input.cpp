//
//  get-input.cpp
//  ego
//
//  Created by Andy Carlos on 8/10/19.
//  Copyright © 2019 Andy Carlos. All rights reserved.
//

#include "get-input.hpp"
#include <string>
#include <vector>
#include "game-type.hpp"

bool get_input(int & cluster_count, int & cluster_size, int & equilibrium_population_size, int & training_time, Game_Type & game, bool & merge_output_input_layers, bool & wire_internally, int & population_size, int & generations_to_equilibrium)
{
	std::string input_str = "";
	int input {0};
	std::cout << "What game to play? (\"curve\", \"dots\") " << std::flush;
	std::string game_s = get_valid_input(std::vector<std::vector<std::string>>{{"curve"}, {"dots"}});
	if (game_s == "curve") game = Game_Type::curve;
	else if (game_s == "dots") game = Game_Type::dots;
	
	std::cout << "How many neuron clusters? " << std::flush;
	do std::cin >> input; while (input < 0);
	cluster_count = input;
	
	std::cout << "How many neurons " << (cluster_count > 0 ? "per cluster? " : "in addition to input neurons? ") << std::flush;
	do std::cin >> input; while (input<0);
	cluster_size = input;
	if(input < 1)
	{
		cluster_count = 0;
		cluster_size = 0;
	}
	
	if (cluster_count == 0)
	{
		std::cout << "Merge output and input layers? (\"[y]es\", \"[n]o\") " << std::flush;
		merge_output_input_layers = get_valid_input(std::vector<std::vector<std::string>> {{"yes", "y"}, {"no", "n"}}) == "yes" ? true : false;
	}
	else merge_output_input_layers = false;
	
	std::cout << "Wire clusters internally? (\"[y]es\", \"[n]o\") " << std::flush;
	wire_internally = (get_valid_input(std::vector<std::vector<std::string>> {{"yes", "y"}, {"no", "n"}}) == "yes") ? true : false;
	
	std::cout << "How large should the initial population be? " << std::flush;
	do std::cin >> input; while(input<0);
	if(input == 0) {std::cout << "OK, nothing to do... bye!" << std::endl; return false;}
	population_size = input;
	
	std::cout << "How large should the equilibrium population be? " << std::flush;
	do std::cin >> input; while(input<0);
	if(input == 0) {std::cout << "OK, nothing to do... bye!" << std::endl; return false;}
	equilibrium_population_size = input;
	
	if (equilibrium_population_size < population_size)
	{
		std::cout << "How many generations to reach equilibrium population? " << std::flush;
		do std::cin >> input; while(input<0);
		if(input == 0) {std::cout << "OK, nothing to do... bye!" << std::endl; return false;}
		generations_to_equilibrium = input;
	}
	else generations_to_equilibrium = 1;
	
	std::cout << "And how long should I train the neural networks (number of generations)? " << std::flush;
	do std::cin >> input; while(input<0 || (input > 0 && input < generations_to_equilibrium));
	if(input == 0) {std::cout << "OK, nothing to do... bye!" << std::endl; return false;}
	training_time = input;
	
	std::cout << "OK, thinking...!" << std::endl;
	
	return true;
}
