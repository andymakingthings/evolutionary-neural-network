//
//  get-input.hpp
//  ego
//
//  Created by Andy Carlos on 8/10/19.
//  Copyright © 2019 Andy Carlos. All rights reserved.
//

#ifndef get_input_hpp
#define get_input_hpp

#include <iostream>
#include "game-type.hpp"

template <typename T_Container>
std::pair<bool, typename T_Container::value_type::value_type> input_is_valid (typename T_Container::value_type::value_type const & input, T_Container const & valid_inputs)
{
	for (auto &set : valid_inputs) for (auto &v : set)
		if (input == v) return std::make_pair(true, set.front());
	return std::make_pair(false, valid_inputs.back().back());
}

template <typename T_Container>
typename T_Container::value_type::value_type get_valid_input (T_Container valid_inputs)
{
	typename T_Container::value_type::value_type input;
	std::pair<bool, typename T_Container::value_type::value_type> check_valid;
	check_valid.first = false;
	do
	{
		std::cin >> input;
		check_valid = input_is_valid(input, valid_inputs);
	} while (!check_valid.first);
	return check_valid.second;
}

bool get_input(int & cluster_count, int & cluster_size, int & equilibrium_population_size, int & training_time, Game_Type & game, bool & merge_output_input_layers, bool & wire_internally, int & population_size, int & generations_to_equilibrium);

#endif /* get_input_hpp */
