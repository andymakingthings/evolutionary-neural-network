//
//  graphics.cpp
//  ego
//

#include "graphics.hpp"
#include <cmath>
#include <vector>
#include <iostream>
#include "game-type.hpp"
#include <tuple>

void draw_rectangle(SDL_Renderer * renderer, ego::Rect & rectangle) {
	auto x = rectangle.sdl_rect.x;
	auto y = rectangle.sdl_rect.y;
	rectangle.sdl_rect.x = rectangle.sdl_rect.x - rectangle.sdl_rect.w/2;
	rectangle.sdl_rect.y = rectangle.sdl_rect.y - rectangle.sdl_rect.h/2;
	SDL_SetRenderDrawColor ( renderer, rectangle.sdl_color.r, rectangle.sdl_color.g, rectangle.sdl_color.b, rectangle.sdl_color.a );
	SDL_RenderFillRect( renderer, &rectangle.sdl_rect );
	rectangle.sdl_rect.x = x;
	rectangle.sdl_rect.y = y;
}

void draw_circle (SDL_Renderer * renderer, int x, int y, double radius, SDL_Color color, int corners) {
	SDL_SetRenderDrawColor ( renderer, color.r, color.g, color.b, color.a );
	
	std::vector<SDL_Point> points;
	const double angle_increment = 2*3.14159265/corners;
	double angle = 0;
	for (int i = 0; i < corners; ++i)
	{
		int x_ = x + radius*std::cos(angle);
		int y_ = y + radius*std::sin(angle);
		points.emplace_back();
		points.back().x = x_;
		points.back().y = y_;
		angle += angle_increment;
	}
	
	for (auto p = points.begin(); p != points.end(); ++p)
	{
		auto p_next = p; ++p_next;
		if (p_next == points.end()) p_next = points.begin();
		SDL_RenderDrawLine(renderer, (*p).x, (*p).y, (*p_next).x, (*p_next).y);
	}
}

ego::SDL_Game_Package::SDL_Game_Package(Game_Type const & game_type) {
	switch (game_type) {
		case Game_Type::curve:
			sdl_game_activity.title = "Fit the Curve";
			sdl_game_activity.width = 680;
			sdl_game_activity.height = 340;
			break;
		case Game_Type::dots:
			sdl_game_activity.title = "Chase the Dots, Avoid the Walls";
			sdl_game_activity.width = 340;
			sdl_game_activity.height = 340;
			break;
		default:
			sdl_game_activity.width = 340;
			sdl_game_activity.height = 340;
			break;
	}
	
	sdl_neural_activity.title = "Network Graph";
	sdl_neural_activity.width = 680;
	sdl_neural_activity.height = 680;
}

bool ego::SDL_Game_Package::initialize_sdl_windows() {
	// make a graphics window
	sdl_game_activity.window = SDL_CreateWindow(sdl_game_activity.title.c_str(), 100, 100, sdl_game_activity.width, sdl_game_activity.height, SDL_WINDOW_SHOWN);
	if (sdl_game_activity.window == nullptr) {
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return false;
	}
	sdl_game_activity.renderer = SDL_CreateRenderer(sdl_game_activity.window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (sdl_game_activity.renderer == nullptr) {
		SDL_DestroyWindow(sdl_game_activity.window);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return false;
	}
	
	// make another graphics window for the network graph
	sdl_neural_activity.window = SDL_CreateWindow(sdl_neural_activity.title.c_str(), 100, 100, sdl_neural_activity.width, sdl_neural_activity.height, SDL_WINDOW_SHOWN);
	if (sdl_neural_activity.window == nullptr) {
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return false;
	}
	sdl_neural_activity.renderer = SDL_CreateRenderer(sdl_neural_activity.window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (sdl_neural_activity.renderer == nullptr)
	{
		SDL_DestroyWindow(sdl_neural_activity.window);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return false;
	}
	return true;
}

void ego::SDL_Game_Package::destroy_sdl_windows() {
	SDL_DestroyWindow(sdl_game_activity.window);
	SDL_DestroyWindow(sdl_neural_activity.window);
}

ego::SDL_Game_Package::~SDL_Game_Package() {
	destroy_sdl_windows();
}
