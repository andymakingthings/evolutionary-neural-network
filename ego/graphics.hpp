//
//  graphics.hpp
//  ego
//

#ifndef __ego__graphics__
#define __ego__graphics__

#include <SDL2/SDL.h>
#include "game-type.hpp"
#include <string>

namespace ego {

class Rect
{
public:
	SDL_Rect sdl_rect;
	SDL_Color sdl_color;
};

class SDL_Display_Package
{
public:
	SDL_Renderer * renderer {nullptr};
	SDL_Window * window {nullptr};
	std::string title{""};
	int width {0};
	int height {0};
};

class SDL_Game_Package
{
public:
	Game_Type game_type;
	SDL_Display_Package sdl_game_activity;
	SDL_Display_Package sdl_neural_activity;
	SDL_Game_Package(Game_Type const &);
	bool initialize_sdl_windows();
	void destroy_sdl_windows();
	~SDL_Game_Package();
};
} // namespace ego

void draw_rectangle(SDL_Renderer *, ego::Rect &);

void draw_circle (SDL_Renderer * renderer, int x, int y, double radius, SDL_Color color, int corners);

#endif /* defined(__ego__graphics__) */
