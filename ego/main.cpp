//
//  main.cpp
//  ego
//

#include <iostream>
#include "evolve.hpp"
#include "game-type.hpp"
#include "graphics.hpp"
#include "organism.hpp"
#include "gooey-lib/misc.hpp"
#include "display-network.hpp"
#include "avoid-the-walls.hpp"

#include <nlohmann/json.hpp>
#include <string>

int main(int argc, char **argv) {
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 0;
	}
	
	//evolve();
	
	auto game = ego::SDL_Game_Package(Game_Type::dots);
	game.sdl_game_activity.width *= 2;
	game.sdl_game_activity.height *= 2;
	game.initialize_sdl_windows();
	
	auto json = gooey::misc::slurp_textfile("best-490.json");
	//auto j = nlohmann::json::parse(json);
	auto e = Animal(json);
	display_network(e.brain, game.sdl_neural_activity);
	avoid_the_walls(e.brain, 1000, game.sdl_game_activity, true, true);
	
	SDL_Quit();
	return 0;
}
