//
//  neural-lattice.cpp
//  ego
//

#include "neural-lattice.hpp"
#include <iostream>

void Neural_Lattice::pulse(unsigned long long int n)
{
	for (int i = 0; i < n; ++i)
	{
		for (auto &c : cluster) c.fire();
		for (auto &c : cluster) c.rest();
	}
}

void Neural_Lattice::forget()
{
	for (auto &c : cluster) c.forget();
}

void Neural_Lattice::mutate()
{
	for (auto &c : cluster) c.mutate();
}

void Neural_Lattice::randomize_contents()
{
	for (auto &c : cluster) c.randomize_contents();
}

Neural_Lattice::Neural_Lattice(Neural_Lattice const & female, Neural_Lattice const & male)// : cluster(female.cluster)
{
	auto c_m = male.cluster.begin();
	for (auto &c_f : female.cluster)
	{
		bool found {false};
		auto c_m_f = c_m;
		for (c_m_f = c_m; c_m_f != male.cluster.end(); ++c_m_f)
			if ((*c_m_f).get_id() == c_f.get_id())
			{
				found = true;
				break;
			}
		if (!found) for (c_m_f = male.cluster.begin(); c_m_f != c_m; ++c_m_f)
			if ((*c_m_f).get_id() == c_f.get_id())
			{
				found = true;
				break;
			}
		
		if (found) cluster.emplace_back(c_f, *c_m_f);
		else
		{
			std::cout << "male cluster not found! relying on female only.\n";
			cluster.emplace_back(c_f);
		}
		
		++c_m;
	}
	
	// update neuron targets
	std::map<unsigned long long int, Neural_Network *> networks;
	for (auto &c : cluster) networks.emplace(c.get_id(), &c);
	
	Axon_Option options;
	options.prune_option = Axon_Option::Prune::all_unmatched;
	options.skip_update_option = Axon_Option::Skip_Update::none;
	options.update_option = Axon_Option::Update::every_axon;
	
	for (auto &c : cluster) c.update_targets(networks, options);
}

// copy constructor
Neural_Lattice::Neural_Lattice(Neural_Lattice const & copy_from) : cluster(copy_from.cluster)
{
	// update neuron targets
	std::map<unsigned long long int, Neural_Network *> networks;
	for (auto &c : cluster) networks.emplace(c.get_id(), &c);
	
	
	auto options = Axon_Option(Axon_Option::Update::every_axon,
							   Axon_Option::Skip_Update::none,
							   Axon_Option::Prune::none);
	
	for (auto &c : cluster) c.update_targets(networks, options);
}

// copy assignment
Neural_Lattice & Neural_Lattice::operator= (Neural_Lattice const & copy_from)
{
	cluster = copy_from.cluster;
	
	// update neuron targets
	std::map<unsigned long long int, Neural_Network *> networks;
	for (auto &c : cluster) networks.emplace(c.get_id(), &c);
	
	auto options = Axon_Option(Axon_Option::Update::every_axon,
							   Axon_Option::Skip_Update::none,
							   Axon_Option::Prune::none);
	
	for (auto &c : cluster) c.update_targets(networks, options);
	
	return *this;
}

// save to JSON string
std::string Neural_Lattice::to_json()
{
	//std::vector<Neural_Network> cluster;
	std::string json = "{\"clusters\":[";
	int i = 0;
	for (auto &c : cluster) {
		if (i == 0) json += c.to_json();
		else json += "," + c.to_json();
		++i;
	}
	json += "]}";
	return json;
}

// json constructor
Neural_Lattice::Neural_Lattice(nlohmann::json const & json) {
	for (auto &j_cluster : json["clusters"]) {
		cluster.emplace_back(Neural_Network(j_cluster));
	}
	// todo: clean up copy-paste from copy constructor
	// update neuron targets
	std::map<unsigned long long int, Neural_Network *> networks;
	for (auto &c : cluster) networks.emplace(c.get_id(), &c);
	auto options = Axon_Option(Axon_Option::Update::every_axon,
							   Axon_Option::Skip_Update::none,
							   Axon_Option::Prune::none);
	for (auto &c : cluster) c.update_targets(networks, options);
}
