//
//  neural-lattice.hpp
//  ego
//

#ifndef neural_lattice_hpp
#define neural_lattice_hpp

#include "neural-network.hpp"
#include <string>
#include <nlohmann/json.hpp>

class Neural_Lattice
{
public:
	std::vector<Neural_Network> cluster;
	void pulse(unsigned long long int n = 1);
	void forget();
	void mutate();
	void randomize_contents();
	
	// default constructor
	Neural_Lattice() = default;
	
	// copy constructor
	Neural_Lattice(Neural_Lattice const &);
	
	// copy assignment
	Neural_Lattice & operator= (Neural_Lattice const &);
	
	// mating constructor
	Neural_Lattice(Neural_Lattice const & female, Neural_Lattice const & male);
	
	// constructor from JSON
	//Neural_Lattice(std::string json);
	
	// serialize to JSON
	std::string to_json();
	
	Neural_Lattice(nlohmann::json const &);
};

#endif /* neural_lattice_hpp */
