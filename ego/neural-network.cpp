//
//  neural.cpp
//  ego
//

#include "neural-network.hpp"
#include "neuron.hpp"
#include "axon.hpp"

#include <random>
#include <map>
#include <vector>
#include <utility>
#include <gooey-lib/math.hpp>
#include <gooey-lib/io.hpp>
#include <iostream>

unsigned long long int Neural_Network::get_id() const
{
	return id;
}

void Neural_Network::set_id(unsigned long long int x)
{
	id = x;
}

Neuron * Neural_Network::get_neuron_by_id(unsigned long long int x) const
{
	auto it = neuron_id_map.find(x);
	if (it != neuron_id_map.end()) return it->second;
	else return nullptr;
}

unsigned long long int Neural_Network::generate_id()
{
	return max_id++;
}

double Neural_Network::get_meta_mutation_rate() const
{
	return mutation_rate_meta;
}

double Neural_Network::get_mutation_rate() const
{
	return mutation_rate;
}

double Neural_Network::get_sigma() const
{
	return mutation_rate_sigma;
}

double Neural_Network::get_suppressor_mutation_rate() const
{
	return mutation_rate_suppressor_toggle;
}

double Neural_Network::get_signal_type_mutation_rate() const
{
	return mutation_rate_signal_type_toggle;
}

double Neural_Network::get_accumulate_type_mutation_rate() const
{
	return mutation_rate_accumulate_type_toggle;
}

double Neural_Network::get_self_axon_mutation_rate() const
{
	return mutation_rate_self_axon_toggle;
}

void Neural_Network::set_meta_mutation_rate(double x)
{
	mutation_rate_meta = x;
}

void Neural_Network::set_mutation_rate(double x)
{
	mutation_rate = x;
}

void Neural_Network::set_sigma(double x)
{
	mutation_rate_sigma = x;
}

void Neural_Network::set_suppressor_mutation_rate(double x)
{
	mutation_rate_suppressor_toggle = x;
}

void Neural_Network::set_signal_type_mutation_rate(double x)
{
	mutation_rate_signal_type_toggle = x;
}

void Neural_Network::set_accumulate_type_mutation_rate(double x)
{
	mutation_rate_accumulate_type_toggle = x;
}

void Neural_Network::set_self_axon_mutation_rate(double x)
{
	mutation_rate_self_axon_toggle = x;
}

void Neural_Network::grow_neuron(unsigned long long int n)
{
	while (n > 0)
	{
		neurons.emplace_back(new Neuron(generate_id()));
		neurons.back()->set_parent_network(this);
		neuron_id_map.emplace(neurons.back()->get_id(), neurons.back().get());
		--n;
	}
}

void Neural_Network::update_targets(std::map<unsigned long long int, Neural_Network *> & networks, Axon_Option options)
{
	for (auto &n : neurons) for (auto &a : n->axons()) a->update_target(networks, options);
}

void Neural_Network::update_targets(Neural_Network * network, Axon_Option update_options)
{
	std::map<unsigned long long int, Neural_Network *> networks_list;
	networks_list.emplace(network->get_id(), network);
	
	update_targets(networks_list, update_options);
}

void Neural_Network::update_targets(Axon_Option update_options)
{
	update_targets(this, update_options);
}

void Neural_Network::enable_self_axons(bool b)
{
	for (auto &n : neurons) n->enable_self_axon(b);
}

void Neural_Network::forget()
{
	for (auto &n : neurons) n->forget();
}

void Neural_Network::fire()
{
	for (auto &n : neurons) n->fire();
}

void Neural_Network::rest()
{
	for (auto &n : neurons) n->rest();
}

void Neural_Network::pulse(unsigned long long int n)
{
	while (n > 0)
	{
		fire();
		rest();
		--n;
	}
}

// randomize contents according to given parameters: will not change
// base mutation rates, etc
void Neural_Network::randomize_contents(double sigma, double probability_suppressor, double probability_signal_type, double probability_accumulate_type, double probability_self_axon)
{
	// sanitize mutation_rate: it must be ≥ 0.
	if(sigma < 0) sigma = 0;
	std::random_device rd;
	std::mt19937 toss(rd());
	std::lognormal_distribution<> die(0, sigma);
	std::bernoulli_distribution coin_suppressor(probability_suppressor);
	std::uniform_int_distribution<> die_signal_type(0, static_cast<int>(Neuron::Signal_Type::count)-1);
	std::uniform_int_distribution<> die_accumulate_type(0, static_cast<int>(Neuron::Accumulate_Type::count)-1);
	std::bernoulli_distribution coin_self_axon(probability_self_axon);
	std::uniform_real_distribution<double> coin_fraction(std::numeric_limits<double>::min(), 1);
	
	// attributes are randomly set depending on sigma (variation)
	for (auto &n : neurons)
	{
		n->set_threshold(die(toss));
		n->set_sigmoid(die(toss));
		n->set_suppression(coin_suppressor(toss));
		n->set_weight(die(toss));
		n->set_equilibrium_rate(coin_fraction(toss));
		n->set_signal_type( static_cast<Neuron::Signal_Type>(die_signal_type(toss)) );
		n->set_accumulate_type( static_cast<Neuron::Accumulate_Type>(die_accumulate_type(toss)) );
		n->enable_self_axon(coin_self_axon(toss));
		for (auto &a : n->axons())
		{
			a->set_weight(die(toss));
			// EXPERIMENTAL
			a->set_suppression(coin_suppressor(toss));
		}
	}
}

// randomize contents based on base mutation rates, etc
void Neural_Network::randomize_contents()
{
	randomize_contents(mutation_rate_sigma, mutation_rate_suppressor_toggle, mutation_rate_signal_type_toggle, mutation_rate_accumulate_type_toggle, mutation_rate_self_axon_toggle);
}

// randomize contents & self (mutation rates, etc)
void Neural_Network::randomize(double sigma)
{
	// sanitize mutation_rate: it must be ≥ 0.
	if(sigma < 0) sigma = 0;
	std::random_device rd;
	std::mt19937 toss(rd());
	std::lognormal_distribution<> die(0, sigma);
	std::uniform_real_distribution<> prob(0,1);
	
	set_sigma(die(toss));
	set_meta_mutation_rate(prob(toss));
	set_mutation_rate(prob(toss));
	set_suppressor_mutation_rate(prob(toss));
	
	randomize_contents(sigma, prob(toss), prob(toss), prob(toss), prob(toss));
}

// 3. randomize self & contents according to given params randomize(sigma)

void Neural_Network::mutate(double rate, double sigma, double probability_toggle_suppressor, double probability_toggle_signal_type, double probability_toggle_accumulate_type, double probability_toggle_self_axon)
{
	// sanitize sigma: it must be ≥ 0.
	if(sigma < 0) sigma = 0;
	std::random_device rd;
	std::mt19937 toss(rd());
	std::lognormal_distribution<> die(0, sigma);
	std::bernoulli_distribution coin(rate);
	std::bernoulli_distribution toggle_suppressor(probability_toggle_suppressor);
	std::uniform_int_distribution<> die_signal_type(0, static_cast<int> (Neuron::Signal_Type::count)-1);
	std::uniform_int_distribution<> die_accumulate_type(0, static_cast<int> (Neuron::Accumulate_Type::count)-1);
	std::bernoulli_distribution toggle_signal_type(probability_toggle_signal_type);
	std::bernoulli_distribution toggle_accumulate_type(probability_toggle_accumulate_type);
	std::bernoulli_distribution toggle_self_axon(probability_toggle_self_axon);
	
	// attributes are randomly modified more or less depending on scale
	for (auto &n : neurons)
	{
		if (coin(toss)) if (toggle_suppressor(toss))
			n->set_suppression(!n->is_suppressor());
		if (coin(toss)) if (toggle_self_axon(toss))
			n->enable_self_axon(!n->enabled_self_axon());
		if (coin(toss)) if (toggle_signal_type(toss))
			n->set_signal_type(static_cast<Neuron::Signal_Type> (die_signal_type(toss)));
		if (coin(toss)) if (toggle_accumulate_type(toss))
			n->set_accumulate_type(static_cast<Neuron::Accumulate_Type> (die_accumulate_type(toss)));
		if (coin(toss)) n->set_threshold(n->get_threshold()*die(toss));
		if (coin(toss)) n->set_equilibrium_rate (
			gooey::lineage::Mutable_Range::product_walk_probability (
			n->get_equilibrium_rate(), die(toss)));
		if (coin(toss))	n->set_sigmoid(n->get_sigmoid()*die(toss));
		if (coin(toss)) n->set_weight(n->get_weight()*die(toss));
		//if (coin(toss)) for (auto &a : n->axons())
		//	a->set_weight(a->get_weight()*die(toss));
		for (auto &a : n->axons()) if (coin(toss))
		{
			a->set_weight(a->get_weight()*die(toss));
			// EXPERIMENTAL
			if (toggle_suppressor(toss)) a->set_suppression(!a->is_suppressor());
		}
	}
	
	// mutate self-attributes as well
	if (coin(toss)) set_sigma(mutation_rate_sigma.product_walk(die(toss)));
	if (coin(toss)) set_suppressor_mutation_rate (mutation_rate_suppressor_toggle.product_walk(die(toss)));

	std::bernoulli_distribution coin2(gooey::math::fix_within_minimum(rate, .05));
	std::bernoulli_distribution coin3(get_meta_mutation_rate());
	if (coin2(toss)) set_meta_mutation_rate (mutation_rate_meta.product_walk(die(toss)));
	if (coin3(toss)) set_mutation_rate (mutation_rate.product_walk(die(toss)));
}

void Neural_Network::mutate()
{
	mutate(mutation_rate, mutation_rate_sigma, mutation_rate_suppressor_toggle, mutation_rate_signal_type_toggle, mutation_rate_accumulate_type_toggle, mutation_rate_self_axon_toggle);
}


// copy constructor
Neural_Network::Neural_Network(Neural_Network const & copy_from) : id(copy_from.id), max_id(copy_from.max_id), mutation_rate_meta(copy_from.mutation_rate_meta), mutation_rate(copy_from.mutation_rate), mutation_rate_sigma(copy_from.mutation_rate_sigma), mutation_rate_suppressor_toggle(copy_from.mutation_rate_suppressor_toggle), mutation_rate_signal_type_toggle(copy_from.mutation_rate_signal_type_toggle), mutation_rate_accumulate_type_toggle(copy_from.mutation_rate_accumulate_type_toggle), mutation_rate_self_axon_toggle(copy_from.mutation_rate_self_axon_toggle)
{
	for (auto &n : copy_from.neurons)
	{
		neurons.emplace_back(new Neuron(*n));
		neurons.back()->set_parent_network(this);
		neuron_id_map.emplace(neurons.back()->get_id(), neurons.back().get());
	}
	
	// now update all of the axons to their new targets
	// we don't have a list of networks, so only update those which point
	// internally.
	auto options = Axon_Option(Axon_Option::Update::if_internal_axon,
							   Axon_Option::Skip_Update::none,
							   Axon_Option::Prune::none);
	
	update_targets(this, options);
}

// copy assignment operator
Neural_Network & Neural_Network::operator= (Neural_Network const & rhs)
{
	id = rhs.id;
	max_id = rhs.max_id;
	mutation_rate_meta = rhs.mutation_rate_meta;
	mutation_rate = rhs.mutation_rate;
	mutation_rate_sigma = rhs.mutation_rate_sigma;
	mutation_rate_suppressor_toggle = rhs.mutation_rate_suppressor_toggle;
	mutation_rate_signal_type_toggle = rhs.mutation_rate_signal_type_toggle;
	mutation_rate_accumulate_type_toggle = rhs.mutation_rate_accumulate_type_toggle;
	mutation_rate_self_axon_toggle = rhs.mutation_rate_self_axon_toggle;
	neuron_id_map.clear();
	neurons.clear();
	
	// std::vector <std::unique_ptr<Neuron>> neurons;
	// std::map <unsigned long long int, Neuron *> neuron_id_map;
	for (auto &n : rhs.neurons)
	{
		neurons.emplace_back(new Neuron(*n));
		neurons.back()->set_parent_network(this);
		neuron_id_map.emplace(neurons.back()->get_id(), neurons.back().get());
	}
	
	// now update all of the axons to their new targets
	// we don't have a list of networks, so only update those which point
	// internally.
	auto options = Axon_Option(Axon_Option::Update::if_internal_axon,
							   Axon_Option::Skip_Update::none,
							   Axon_Option::Prune::none);
	
	update_targets(this, options);
	return *this;
}

// mating constructor
Neural_Network::Neural_Network(Neural_Network const & female, Neural_Network const & male) : Neural_Network(female)
{
	// egg starts with female's DNA, which is randomly replaced/recombined with
	// male's DNA to inherit half from both parents.
	
	std::random_device rd;
	std::mt19937 toss(rd());
	std::bernoulli_distribution coin(0.5);
	
	if (coin(toss)) mutation_rate_meta = male.mutation_rate_meta;
	if (coin(toss)) mutation_rate = male.mutation_rate;
	if (coin(toss)) mutation_rate_sigma = male.mutation_rate_sigma;
	if (coin(toss)) mutation_rate_suppressor_toggle = male.mutation_rate_suppressor_toggle;
	if (coin(toss)) mutation_rate_signal_type_toggle = male.mutation_rate_signal_type_toggle;
	if (coin(toss)) mutation_rate_accumulate_type_toggle = male.mutation_rate_accumulate_type_toggle;
	if (coin(toss)) mutation_rate_self_axon_toggle = male.mutation_rate_self_axon_toggle;
	
	for (auto &n : neurons)
	{
		// first check to see if the male has the corresponding neuron
		auto n_male = male.get_neuron_by_id(n->get_id());
		if (n_male != nullptr)
		{
			// recombine
			if (coin(toss)) n->set_suppression(n_male->is_suppressor());
			if (coin(toss)) n->set_threshold(n_male->get_threshold());
			if (coin(toss)) n->set_sigmoid(n_male->get_sigmoid());
			if (coin(toss)) n->set_weight(n_male->get_weight());
			if (coin(toss)) n->set_equilibrium_rate(n_male->get_equilibrium_rate());
			if (coin(toss)) n->set_signal_type(n_male->get_signal_type());
			if (coin(toss)) n->set_accumulate_type(n_male->get_accumulate_type());
			if (coin(toss)) n->enable_self_axon(n_male->enabled_self_axon());
			// axons
			for (auto &a : n->axons())
			{
				auto a_male = n_male->get_axon_by_address(a->get_neural_address());
				if (a_male != nullptr)
				{
					if (coin(toss)) a->set_weight(a_male->get_weight());
					if (coin(toss)) a->set_suppression(a_male->is_suppressor());
				}
			}
		}
	}
}

Neural_Network::Neural_Network()
{
	mutation_rate_meta.set_min(.005);
	mutation_rate_meta.set_max(.9);
	
	mutation_rate.set_min(.001);
	mutation_rate.set_max(.99);
	
	mutation_rate_suppressor_toggle.set_min(.005);
	mutation_rate_suppressor_toggle.set_max(.95);
	
	mutation_rate_signal_type_toggle.set_min(.005);
	mutation_rate_signal_type_toggle.set_max(.95);

	mutation_rate_accumulate_type_toggle.set_min(.005);
	mutation_rate_accumulate_type_toggle.set_max(.95);
	
	mutation_rate_self_axon_toggle.set_min(.005);
	mutation_rate_self_axon_toggle.set_max(.95);
	
	mutation_rate_sigma.set_min(.005);
	mutation_rate_sigma.set_max(std::numeric_limits<double>::max());
}

// serialize
std::string Neural_Network::to_json() {
	//std::vector<Neural_Network> cluster;
	std::string json = "{";
	json = json
		+ "\"id\":\"" + gooey::io::serialize(id) + "\","
		+ "\"max_id\":\"" + gooey::io::serialize(max_id) + "\","
		+ "\"mutation_rate_meta\":" + mutation_rate_meta.to_json() + ","
		+ "\"mutation_rate\":" + mutation_rate.to_json() + ","
		+ "\"mutation_rate_suppressor_toggle\":" + mutation_rate_suppressor_toggle.to_json() + ","
		+ "\"mutation_rate_signal_type_toggle\":" + mutation_rate_signal_type_toggle.to_json() + ","
		+ "\"mutation_rate_accumulate_type_toggle\":" + mutation_rate_accumulate_type_toggle.to_json() + ","
		+ "\"mutation_rate_self_axon_toggle\":" + mutation_rate_self_axon_toggle.to_json() + ","
		+ "\"mutation_rate_sigma\":" + mutation_rate_sigma.to_json() + ","
		+ "\"neurons\":[";
	int i = 0;
	for (auto &n : neurons) {
		if (i == 0) json += n->to_json();
		else json += "," + n->to_json();
		++i;
	}
	json += "]}";
	return json;
}

// json constructor
Neural_Network::Neural_Network(nlohmann::json const & json) {
	id = std::stoull((std::string)json["id"]);
	max_id = std::stoull((std::string)json["max_id"]);
	mutation_rate_meta = gooey::lineage::Mutable_Range::from_json(json["mutation_rate_meta"]);
	mutation_rate = gooey::lineage::Mutable_Range::from_json(json["mutation_rate"]);
	mutation_rate_suppressor_toggle = gooey::lineage::Mutable_Range::from_json(json["mutation_rate_suppressor_toggle"]);
	mutation_rate_signal_type_toggle = gooey::lineage::Mutable_Range::from_json(json["mutation_rate_signal_type_toggle"]);
	mutation_rate_accumulate_type_toggle = gooey::lineage::Mutable_Range::from_json(json["mutation_rate_accumulate_type_toggle"]);
	mutation_rate_self_axon_toggle = gooey::lineage::Mutable_Range::from_json(json["mutation_rate_self_axon_toggle"]);
	mutation_rate_sigma = gooey::lineage::Mutable_Range::from_json(json["mutation_rate_sigma"]);
	
	for (auto &j_neuron : json["neurons"]) {
		neurons.emplace_back(new Neuron(j_neuron));
		neurons.back()->set_parent_network(this);
		neuron_id_map.emplace(neurons.back()->get_id(), neurons.back().get());
	}
	// todo: clean up copy-paste from copy constructor
	
	// now update all of the axons to their new targets
	// we don't have a list of networks, so only update those which point
	// internally.
	auto options = Axon_Option(Axon_Option::Update::if_internal_axon,
							   Axon_Option::Skip_Update::none,
							   Axon_Option::Prune::none);
	
	update_targets(this, options);
}
