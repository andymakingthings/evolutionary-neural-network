//
//  neural.hpp
//  ego
//

#ifndef __ego__neural__
#define __ego__neural__

#include "neuron.hpp"
#include "axon.hpp"
#include <vector>
#include <map>
#include <memory>
#include <string>
#include <gooey-lib/lineage.hpp>
#include <nlohmann/json.hpp>

class Neural_Network
{
private:
	unsigned long long int id;
	unsigned long long int max_id{0};
	unsigned long long int generate_id();
	
	// see constructor for actual limits
	gooey::lineage::Mutable_Range mutation_rate_meta {0}; // (0, 1)
	gooey::lineage::Mutable_Range mutation_rate {0}; // (0, 1)
	gooey::lineage::Mutable_Range mutation_rate_suppressor_toggle {.1}; // (0, 1)
	gooey::lineage::Mutable_Range mutation_rate_signal_type_toggle {.1}; // (0, 1)
	gooey::lineage::Mutable_Range mutation_rate_accumulate_type_toggle {.1}; // (0, 1)
	gooey::lineage::Mutable_Range mutation_rate_self_axon_toggle {.1}; // (0, 1)
	gooey::lineage::Mutable_Range mutation_rate_sigma {0}; // (0, max)
	
	std::map <unsigned long long int, Neuron *> neuron_id_map;
public:
	std::vector<std::unique_ptr<Neuron>> neurons;
	
	unsigned long long int get_id() const;
	void set_id(unsigned long long int);
	
	double get_meta_mutation_rate() const;
	double get_mutation_rate() const;
	double get_sigma() const;
	double get_suppressor_mutation_rate() const;
	double get_signal_type_mutation_rate() const;
	double get_accumulate_type_mutation_rate() const;
	double get_self_axon_mutation_rate() const;
	
	void set_meta_mutation_rate(double);
	void set_mutation_rate(double);
	void set_sigma(double);
	void set_suppressor_mutation_rate(double);
	void set_signal_type_mutation_rate(double);
	void set_accumulate_type_mutation_rate(double);
	void set_self_axon_mutation_rate(double);
	
	Neuron * get_neuron_by_id(unsigned long long int) const;
	
	void grow_neuron(unsigned long long int n = 1);
	//void prune_neuron();
	
	void update_targets(std::map<unsigned long long int, Neural_Network *> & networks, Axon_Option options = Axon_Option());
	void update_targets(Neural_Network *, Axon_Option update_options = Axon_Option());
	void update_targets(Axon_Option update_options = Axon_Option());
	
	void enable_self_axons(bool);
	
	void forget();
	void fire();
	void rest();
	void pulse(unsigned long long int n = 1);
	void randomize(double sigma);
	void randomize_contents();
	void randomize_contents(double sigma, double probability_suppressor, double probability_signal_type, double probability_accumulate_type, double probability_self_axon);
	void mutate(double rate, double sigma, double probability_toggle_suppressor, double probability_toggle_signal_type, double probability_toggle_accumulate_type, double probability_toggle_self_axon);
	void mutate();
	
	// default constructor
	Neural_Network();
	// copy constructor
	Neural_Network(Neural_Network const &);
	// copy assignment
	Neural_Network & operator= (Neural_Network const &);
	// move constructor
	Neural_Network(Neural_Network &&) = default; // custom define this??
	// using smart pointers: default destructor is fine
	~Neural_Network() = default;
	// special constructor for "mating" two networks in a genetic algorithm
	Neural_Network(Neural_Network const & female, Neural_Network const & male);
	// constructor from json
	//Neural_Network(std::string json);
	// serialize to JSON
	std::string to_json();
	Neural_Network(nlohmann::json const &);
};

#endif /* defined(__ego__neural__) */
