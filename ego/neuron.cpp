//
//  neuron.cpp
//  ego
//

#include "axon.hpp"
#include "neuron.hpp"
#include "neural-network.hpp"
#include <gooey-lib/math.hpp>
#include <cmath>
#include <set>

#include <iostream>

unsigned long long int Neuron::generate_id()
{
	return max_id++;
}

double Neuron::signal() const
{
	double tmp {0};
	switch (signal_type)
	{
		default:
		case Signal_Type::sigmoid:
			// logistic function from -1 to 1
			tmp = std::exp( sigmoid_slope * -get_value() );
			if (tmp == -1) tmp -= std::numeric_limits<double>::min();
			return 2/( 1 + tmp ) - 1;
			break;
			
		/*case Signal_Type::graded_potential :
			return get_value();
			break;
			
		case Signal_Type::sine :
			return std::sin(get_value() + get_weight());
			break;
		
		case Signal_Type::square :
			return get_value() * get_value();
			break;
	
		case Signal_Type::cube :
			return get_value() * get_value() * get_value();
			break;
			
		case Signal_Type::exp :
			return std::exp(get_value());
			break;
			
		case Signal_Type::log :
			return gooey::math::sign(get_value()) * std::log(std::abs(get_value()));
			break;
			
		case Signal_Type::inverse :
			if (get_value() != 0) return 1/get_value();
			else return 0;
			break;*/
	}
}

void Neuron::receive_signal(double signal)
{
	switch (accumulate_type)
	{
		default:
		case (Accumulate_Type::sum) :
			if (is_suppressor()) signal_accumulated -= signal;
			else signal_accumulated += signal;
			break;
		
		case (Accumulate_Type::product) :
			if (!signal_received) signal_accumulated = 1;
			if (signal == 0) break;
			if (is_suppressor())
			{
				signal_accumulated =
					signal_accumulated / signal != 0
					? signal_accumulated / signal
					: gooey::math::sign(signal_accumulated) / gooey::math::sign(signal) * std::numeric_limits<double>::min();
			}
			else
			{
				signal_accumulated =
					signal_accumulated * signal != 0
					? signal_accumulated * signal
					: gooey::math::sign(signal_accumulated) * gooey::math::sign(signal) * std::numeric_limits<double>::min();
			}
			break;
			
		case (Accumulate_Type::minmax) :
			if (is_suppressor())
				signal_accumulated = std::min(signal_accumulated, signal);
			else
				signal_accumulated = std::max(signal_accumulated, signal);
			break;
	}
	
	// -inf < signal_accumulated < inf
	signal_accumulated = gooey::math::fix_within_bounds( signal_accumulated,
		std::numeric_limits<double>::lowest(),
		std::numeric_limits<double>::max());
	
	signal_received = true;
}

unsigned long long int Neuron::get_id() const
{
	return id;
}

void Neuron::set_id(unsigned long long int x)
{
	id = x;
}

Neural_Network * Neuron::get_parent_network() const
{
	return parent_network;
}

void Neuron::set_parent_network(Neural_Network * new_parent)
{
	parent_network = new_parent;
	if (parent_network != nullptr) address.network_id = parent_network->get_id();
}

Neural_Address const & Neuron::get_neural_address() const
{
	return address;
}

void Neuron::set_neural_address(Neural_Address const & new_address)
{
	address = new_address;
	for (auto &a : axons_receiving)
		a->receive_message(Axon::Message::address_change, Axon::Relation::neighbor, this);
}

bool Neuron::enabled_self_axon() const
{
	return allow_self_axon;
}

void Neuron::enable_self_axon(bool b)
{
	allow_self_axon = b;
}

Neuron::Signal_Type Neuron::get_signal_type() const
{
	return signal_type;
}

void Neuron::set_signal_type(Signal_Type t)
{
	signal_type = t;
}

Neuron::Accumulate_Type Neuron::get_accumulate_type() const
{
	return accumulate_type;
}

void Neuron::set_accumulate_type(Accumulate_Type t)
{
	accumulate_type = t;
}

bool Neuron::is_suppressor() const
{
	return suppression;
}

void Neuron::set_suppression(bool b)
{
	suppression = b;
}

double Neuron::get_threshold() const
{
	return threshold_magnitude;
}

void Neuron::set_threshold(double x)
{
	//  0 < threshold < inf
	threshold_magnitude = gooey::math::fix_within_bounds( x,
		std::numeric_limits<double>::min(),
		std::numeric_limits<double>::max());
}

double Neuron::get_equilibrium_rate() const
{
	return equilibrium_rate;
}

void Neuron::set_equilibrium_rate(double x)
{
	equilibrium_rate = x;
}

double Neuron::get_weight() const
{
	return weight;
}

void Neuron::set_weight(double x)
{
	//  -inf < weight < inf
	weight = gooey::math::fix_within_bounds( x,
		std::numeric_limits<double>::lowest(),
		std::numeric_limits<double>::max());
}

double Neuron::get_sigmoid() const
{
	return sigmoid_slope;
}

void Neuron::set_sigmoid(double slope)
{
	//  0 < sigmoid < inf
	sigmoid_slope = gooey::math::fix_within_bounds( slope,
		std::numeric_limits<double>::min(),
		std::numeric_limits<double>::max());
}

double Neuron::get_value() const
{
	//if (!std::isfinite(value))
	//	std::cout << "(debug: Neuron::get_value) value is not finite.\n";
	return value;
}

void Neuron::push_input(double x)
{
	value = gooey::math::fix_within_bounds( x,
		std::numeric_limits<double>::lowest(),
		std::numeric_limits<double>::max());
}

Axon::Response Neuron::receive_message(Axon::Message message, Axon::Relation relation, Axon * axon, Neuron * neuron)
{
	switch (message)
	{
		case Axon::Message::attaching:
			
			// external axon is attaching to this neuron. add to axons_receiving.
			if (relation == Axon::Relation::neighbor)
			{
				if(axon == nullptr) return Axon::Response::denied;
				axons_receiving.emplace(axon);
				return Axon::Response::permitted;
			}
			
			// child axon is attaching to a neuron. add it to neurons_targeting,
			// or deny permission if already in set
			else if (relation == Axon::Relation::family)
			{
				if (neurons_targeting.count(neuron) || axon == nullptr || neuron == nullptr)
					return Axon::Response::denied;
				neurons_targeting.emplace(neuron, axon);
				return Axon::Response::permitted;
			}
			break;
			
		case Axon::Message::releasing:
			
			// a child axon is detaching from a neuron. remove it from
			// neurons_targeting.
			if (relation == Axon::Relation::family)
			{
				neurons_targeting.erase(neuron);
				return Axon::Response::permitted;
			}
			
			// an external axon, presumably one in axons_receiving, is detaching
			// from this neuron—remove it from the axons_receiving list
			else if (relation == Axon::Relation::neighbor)
			{
				axons_receiving.erase(axon);
				return Axon::Response::permitted;
			}
			break;
			
		case Axon::Message::address_change:
			// one of this neuron's axons is changing the neural address it
			// points to. get the new address.
			if (relation == Axon::Relation::family)
			{
				if (axon == nullptr) return Axon::Response::denied;
				auto new_address = axon->get_neural_address();
				if(neural_addresses_targeting.count(new_address))
					return Axon::Response::denied;
				else neural_addresses_targeting.emplace(address, axon);
				return Axon::Response::permitted;
			}
			
			break;
		case Axon::Message::aptosis:
			break;
	}
	return Axon::Response::confused;
}

std::set <std::unique_ptr<Axon>> const & Neuron::axons() const
{
	return axons_list;
}

Axon * Neuron::axons(Neuron * target_neuron) const
{
	auto it = neurons_targeting.find(target_neuron);
	if (it != neurons_targeting.end()) return it->second;
	else return nullptr;
}

bool Neuron::axon_present(Neuron * target_neuron) const
{
	if (neurons_targeting.find(target_neuron) != neurons_targeting.end())
		return true;
	else return false;
}

Axon * Neuron::get_axon_by_address(Neural_Address const & find_address) const
{
	//std::map <Neural_Address, Axon *> neural_addresses_targeting;
	auto it = neural_addresses_targeting.find(find_address);
	if (it != neural_addresses_targeting.end()) return it->second;
	else return nullptr;
}

void Neuron::grow_axon(Neuron * target_neuron)
{
	if (target_neuron == nullptr || (target_neuron == this && !allow_self_axon)) return;
	
	// duplicate axon (to same neuron as already is targeted) will not occur:
	// axons_list is a set.
	auto pair = axons_list.emplace(std::unique_ptr<Axon>(new Axon));
	if (!pair.second) return; // if unsuccessful
	(*pair.first)->set_id(generate_id());
	
	(*pair.first)->set_parent_neuron(this); // inform axon of lineage
	
	auto target_network = target_neuron->parent_network;
	auto network_id = (target_network == nullptr) ? 0 : target_network->get_id();
	auto target_location = (target_network == parent_network) ? Neural_Address::Location::internal_address : Neural_Address::Location::external_address;
	auto target_address = Neural_Address(network_id, target_neuron->get_id());
	
	// set the axon's target
	(*pair.first)->set_target(target_address, target_neuron, target_neuron->parent_network);
	// set the location
	(*pair.first)->set_location(target_location);
	// tell the axon to set its address, so that it will notify parent neuron
	// of its address. might be a bit silly but it's easy (axon
	// constructor never notified of address because it didn't
	// know its parent/lineage.)
	(*pair.first)->set_neural_address((*pair.first)->get_neural_address());
}

void Neuron::grow_axon(Neural_Address const & target_address)
{
	auto pair = axons_list.emplace(std::unique_ptr<Axon>(new Axon));
	if (!pair.second) return; // if unsuccessful
	
	(*pair.first)->set_parent_neuron(this); // inform axon of lineage
	(*pair.first)->change_neural_address(target_address); // set the address
}

/*void Neuron::prune_axon(Neuron * target_neuron)
{
	auto it = axons_list.find(target_neuron);
	axons_list.erase(axons(target_neuron));
}

void Neuron::prune_axon(Neural_Address const & target_address)
{
	std::set<Axon *> to_prune;
	
	for (auto &i : axons_list)
		if (i->get_neural_address() == target_address) to_prune.emplace(i.get());
	
	for (auto &i : to_prune) axons_list.erase(*i);
}*/

void Neuron::fire()
{
	double fire_sigmoid = false_fire();
	
	if (fire_sigmoid == 0) return;
	
	// push the signal based on value down the pipe of each axon.
	for (auto &a : axons_list) a->fire( fire_sigmoid );
	
	// maybe do something else with the value?
	bind_on_fire();
	
	// value is now depleted
	push_input(0);
}

void Neuron::rest()
{
	// value is now depleted
	push_input (get_value() * get_equilibrium_rate());
	push_input (get_value() + signal_accumulated);
	signal_accumulated = 0;
	signal_received = false;
}

double Neuron::false_fire()
{
	// check to see if the threshold is met first
	if (std::abs(get_value()) < threshold_magnitude) return 0;

	// push the signal based on value down the pipe of each axon.
	//  -inf < signal < inf
	return gooey::math::fix_within_bounds ( signal(),
		std::numeric_limits<double>::lowest(),
		std::numeric_limits<double>::max());
}

void Neuron::forget()
{
	// clears the signal_accumulated and value_var.
	signal_accumulated = 0;
	push_input(0);
}

Neuron::Neuron(unsigned long long int id_init) : id(id_init)
{
	address.neuron_id = id;
}

Neuron::Neuron(Neuron const & copy_from) : id(copy_from.id), max_id(copy_from.max_id), label(copy_from.label), address(copy_from.address), parent_network(nullptr), allow_self_axon(copy_from.allow_self_axon), signal_type(copy_from.signal_type), accumulate_type(copy_from.accumulate_type), suppression(copy_from.suppression), threshold_magnitude(copy_from.threshold_magnitude), equilibrium_rate(copy_from.equilibrium_rate), weight(copy_from.weight),  sigmoid_slope(copy_from.sigmoid_slope), signal_accumulated(copy_from.signal_accumulated), value(copy_from.value)
{
	// axons_receiving is not initialized - no axons are actually attached to
	// this specific (new) neuron.
	
	// neurons_targeting is not initialized - this neuron is not targeting any
	// axons: the axons will have to update this, which can't be done without
	// the context of a neural network to resolve neuron addresses
	
	// axons_list is a container of pointers, so it must be initialized properly
	/** do that here **/
	for (auto &a_copy_from : copy_from.axons_list)
	{
		auto pair = axons_list.emplace(std::unique_ptr<Axon>(new Axon(*a_copy_from)));
		
		(*pair.first)->set_parent_neuron(this); // inform axon of lineage
		(*pair.first)->set_neural_address((*pair.first)->get_neural_address());
	}
}

Neuron::~Neuron()
{
	// tell axons_receiving that they must release: neuron is dying
	for (auto &a : axons_receiving) a->receive_message(Axon::Message::aptosis, Axon::Relation::neighbor, this);
}

void Neuron::bind_on_fire()
{
	return;
}

std::string Neuron::serialize(Signal_Type t) {
	std::string json;
	switch (t) {
		default:
		case Signal_Type::sigmoid:
			json = "sigmoid";
			break;
	}
	return json;
}

std::string Neuron::serialize(Accumulate_Type t) {
	std::string json;
	switch (t) {
		default:
		case Accumulate_Type::sum:
			json = "sum";
			break;
		case Accumulate_Type::product:
			json = "product";
			break;
		case Accumulate_Type::minmax:
			json = "minmax";
			break;
	}
	return json;
}

Neuron::Signal_Type Neuron::Signal_Type_deserialize(std::string const & s) {
	if (s == "sigmoid") return Signal_Type::sigmoid;
	return Signal_Type::sigmoid;
}

Neuron::Accumulate_Type Neuron::Accumulate_Type_deserialize(std::string const & s) {
	std::string json;
	if (s == "sum") return Accumulate_Type::sum;
	else if (s == "product") return Accumulate_Type::product;
	else if (s == "minmax") return Accumulate_Type::minmax;
	return Accumulate_Type::sum;
}

// serialize
std::string Neuron::to_json() {
	std::string json = "{";
	json = json
	+ "\"label\":\"" + label + "\","
	+ "\"id\":\"" + gooey::io::serialize(id) + "\","
	+ "\"max_id\":\"" + gooey::io::serialize(max_id) + "\","
	+ "\"allow_self_axon\":\"" + gooey::io::serialize(allow_self_axon) + "\","
	+ "\"suppression\":\"" + gooey::io::serialize(suppression) + "\","
	+ "\"weight\":\"" + gooey::io::serialize(weight) + "\","
	+ "\"sigmoid_slope\":\"" + gooey::io::serialize(sigmoid_slope) + "\","
	+ "\"signal_accumulated\":\"" + gooey::io::serialize(signal_accumulated) + "\","
	+ "\"value\":\"" + gooey::io::serialize(value) + "\","
	+ "\"threshold_magnitude\":\"" + gooey::io::serialize(threshold_magnitude) + "\","
	+ "\"equilibrium_rate\":" + equilibrium_rate.to_json() + ","
	+ "\"address\":" + address.to_json() + ","
	+ "\"signal_type\":\"" + serialize(signal_type) + "\","
	+ "\"accumulate_type\":\"" + serialize(accumulate_type) + "\","
	+ "\"axons\":[";
	int i = 0;
	for (auto &a : axons_list) {
		if (i == 0) json += (*a).to_json();
		else json += "," + (*a).to_json();
		++i;
	}
	json += "]}";
	return json;
}

Neuron::Neuron(nlohmann::json const & json) {
	label = json["label"];
	id = std::stoull((std::string)json["id"]);
	max_id = std::stoull((std::string)json["max_id"]);
	allow_self_axon = json["allow_self_axon"] == "true";
	suppression = json["suppression"] == "true";
	weight = std::stod((std::string)json["weight"]);
	sigmoid_slope = std::stod((std::string)json["sigmoid_slope"]);
	signal_accumulated = std::stod((std::string)json["signal_accumulated"]);
	value = std::stod((std::string)json["value"]);
	threshold_magnitude = std::stod((std::string)json["threshold_magnitude"]);
	equilibrium_rate = gooey::lineage::Mutable_Range::from_json(json["equilibrium_rate"]);
	address = Neural_Address(json["address"]);
	signal_type = Signal_Type_deserialize(json["signal_type"]);
	accumulate_type = Accumulate_Type_deserialize(json["accumulate_type"]);
	for (auto &j_axon : json["axons"]) {
		auto pair = axons_list.emplace(std::unique_ptr<Axon>(new Axon(j_axon)));
		(*pair.first)->set_parent_neuron(this); // inform axon of lineage
		(*pair.first)->set_neural_address((*pair.first)->get_neural_address());
	}
}
