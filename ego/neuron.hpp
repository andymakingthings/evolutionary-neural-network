//
//  neuron.hpp
//  ego
//

#ifndef __ego__neuron__
#define __ego__neuron__

#include "axon.hpp"
#include <memory>
#include <set>
#include <map>
#include <string>
#include <limits>
#include <gooey-lib/lineage.hpp>
#include <nlohmann/json.hpp>

class Neural_Network;
class Neuron
{
private:
	// this is the neuron's ID, important for axons
	unsigned long long int id{0};
	
	// this is for generating axon ids
	unsigned long long int max_id{0};
	unsigned long long int generate_id();
	
	Neural_Network * parent_network{nullptr};
	Neural_Address address;
	
	// a flag to let axons grow into the parent neuron in a direct loop
	bool allow_self_axon {false};
	
	// if suppression is true, the signal is reversed (negative).
	bool suppression{false};
	
	// threshold_magnitude should always ≥ 0.
	double threshold_magnitude{std::numeric_limits<double>::min()};
	
	// controls the rate of value depletion during rest
	gooey::lineage::Mutable_Range equilibrium_rate;
	
	// weight amplifies the signal sent to the axon (just as the axon's weight
	// functions).
	double weight{1};
	
	// sigmoid_slope should be always be ≥ 0:
	double sigmoid_slope{1};
	
	bool signal_received {false};
	double signal_accumulated {0};
	double value {0};
	
public:

	enum class Signal_Type
	{
		sigmoid,
		/*graded_potential,
		sine,
		square,
		cube,
		exp,
		log,
		inverse,*/
		count
	};

	enum class Accumulate_Type
	{
		sum,
		product,
		minmax,
		count
	};

private:
	Signal_Type signal_type;
	Accumulate_Type accumulate_type;
	
protected:
	double signal() const;
	
	// this is to notify the axons that point to this neuron upon deconstruction.
	std::set <Axon *> axons_receiving;
	
	// this is to efficiently avoid growing multiple axons to the same target,
	// and to efficiently look up an axon by the neuron it points to
	std::map <Neuron *, Axon *> neurons_targeting;
	std::map <Neural_Address, Axon *> neural_addresses_targeting;
	
	// here are the axons growing from this neuron, firing to others.
	// add to list as so: axon_list.emplace(std::unique_ptr<Axon>(new Axon));
	std::set <std::unique_ptr<Axon>> axons_list;

public:
	std::string label;
	unsigned long long int get_id() const;
	void set_id(unsigned long long int);
	
	Neural_Network * get_parent_network() const;
	void set_parent_network(Neural_Network *);
	
	Neural_Address const & get_neural_address() const;
	void set_neural_address(Neural_Address const &);
	
	bool enabled_self_axon() const;
	void enable_self_axon(bool);

	Signal_Type get_signal_type() const;
	void set_signal_type(Signal_Type);

	Accumulate_Type get_accumulate_type() const;
	void set_accumulate_type(Accumulate_Type);
	
	bool is_suppressor() const;
	void set_suppression(bool);
	
	double get_threshold() const;
	void set_threshold(double);
	
	double get_equilibrium_rate() const;
	void set_equilibrium_rate(double);
	
	double get_weight() const;
	void set_weight(double);
	
	double get_sigmoid() const;
	void set_sigmoid(double slope);

	// when an axon sends a signal, it calls receive_signal with the weighted
	// value in the pipe.
	virtual void receive_signal(double signal);
	
	double get_value() const;
	void push_input(double);
	
	Axon::Response receive_message(Axon::Message, Axon::Relation relation, Axon * = nullptr, Neuron * = nullptr);
	
	std::set <std::unique_ptr<Axon>> const & axons() const;
	Axon * axons(Neuron * target_neuron) const;
	bool axon_present(Neuron * target_neuron) const;
	Axon * get_axon_by_address(Neural_Address const &) const;
	
	void grow_axon(Neuron *);
	void grow_axon(Neural_Address const &);
	void prune_axon(Neuron *);
	void prune_axon(Neural_Address const &);
	
	// all neurons must be fired and then rested: all neurons light up, then all
	// move signal_received to value, then all fire again.
	void fire();
	void rest();
	double false_fire();
	
	// clears the signal_received and value_var.
	void forget();
	
	Neuron() = default;
	Neuron(unsigned long long int id);
	Neuron(Neuron const &); // copy constructor needs custom
	Neuron(Neuron &&) = delete; // move constructor... needs custom?
	~Neuron(); // destructor needs custom
	//Neuron & operator= (Neuron const & rhs); // assignment needs custom?
	
	//void set_threshold_sigmoid(double sigmoid_threshold);
	virtual void bind_on_fire();
	
	std::string to_json();
	static std::string serialize(Signal_Type);
	static std::string serialize(Accumulate_Type);
	static Accumulate_Type Accumulate_Type_deserialize(std::string const &);
	static Signal_Type Signal_Type_deserialize(std::string const &);
	Neuron(nlohmann::json const &);
};

#endif /* defined(__ego__neuron__) */
