//
//  organism.cpp
//  ego
//

#include "organism.hpp"
#include <nlohmann/json.hpp>

Animal::Animal(Animal const & female, Animal const & male) : brain(female.brain, male.brain), fitness(0){}

Animal::Animal(Animal const & copy_from) : brain(copy_from.brain), fitness(copy_from.fitness){}

// serialize
std::string Animal::to_json() {
	std::string json = "{";
	json = json
		+ "\"fitness\":\"" + gooey::io::serialize(fitness) + "\","
		+ "\"brain\":" + brain.to_json()
		+  "}";
	return json;
}

Animal::Animal(std::string const & json) : Animal::Animal(nlohmann::json::parse(json)){}

Animal::Animal(nlohmann::json const & json) {
	fitness = std::stod((std::string)json["fitness"]);
	brain = Neural_Lattice(json["brain"]);
}
