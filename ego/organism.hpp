//
//  organism.hpp
//  ego
//

#ifndef __ego__organism__
#define __ego__organism__

#include "neural-lattice.hpp"
#include <nlohmann/json.hpp>

class Animal
{
public:
	Neural_Lattice brain;
	double fitness;

	// default constructor
	Animal() = default;

	// mating constructor
	Animal(Animal const & female, Animal const & male);
	
	Animal(Animal const & copy_from);	
	
	Animal(std::string const & json);
	
	Animal(nlohmann::json const & json);
	
	std::string to_json();
};

#endif /* defined(__ego__organism__) */
